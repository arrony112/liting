
<?php
global $liting_options;
$liting_footer_widget = isset($liting_options['liting_footer_widget']) ? $liting_options['liting_footer_widget'] : 0;
$copyright_text = isset($liting_options['liting_copyright_text']) ? $liting_options['liting_copyright_text'] : 'Copyright © 2019 <a href="#">Liting</a> All Rights Reserved';
$footer_bg = isset($liting_options['liting_footer_bg']['url']) ? $liting_options['liting_footer_bg']['url'] : '0';
?>

<!-- Start Footer bottom Area -->
<footer class="footer1">
<?php if($liting_footer_widget == 1){?>
    <div class="footer-area image_background" data-image-src="<?php echo esc_url($footer_bg);?>">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-4 col-xs-12 footer-widget">
                    <?php
                    if (is_active_sidebar('footer_column_1')) {
                        dynamic_sidebar('footer_column_1');
                    }
                    ?>
                </div>
                <!-- end single footer -->
                <div class="col-md-4 col-sm-4 col-xs-12 footer-widget">
                    <?php
                    if (is_active_sidebar('footer_column_2')) {
                        dynamic_sidebar('footer_column_2');
                    }
                    ?>
                </div>
                <!-- end single footer -->
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <?php
                    if (is_active_sidebar('footer_column_3')) {
                        dynamic_sidebar('footer_column_3');
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>
    <div class="footer-area-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="copyright">
                        <p>
                        <?php echo wp_kses_post($copyright_text);?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
</div>

<?php wp_footer(); ?>
</body>
</html>