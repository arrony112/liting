<?php
/**
 * The template for displaying comments
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package liting
 */
/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if (post_password_required()) {
    return;
}
// You can start editing here -- including this comment!
if (have_comments()) :
    ?>
    <!--Comments Area-->
    <div class="comments-area" id="comments">
        <div class="comments-heading">
        <h3>
                <?php
				$liting_comment_count = get_comments_number();
				if ('1' === $liting_comment_count) {
					printf(
							/* translators: 1: title. */
							esc_html__('1 Comment', 'liting')
					);
				} else {
					printf(// WPCS: XSS OK.
							/* translators: 1: comment count number, 2: title. */
							esc_html(_nx('%1$s Comment', '%1$s Comments ', $liting_comment_count, 'comments title', 'liting'), 'liting'), number_format_i18n($liting_comment_count)
					);
				}
				?>
        </h3>
        </div>
        <?php
        // You can start editing here -- including this comment!
        if (have_comments()) :
            ?>
            <!-- .comments-title -->
            <?php the_comments_navigation(); ?>
            <div class="comments-list">
                <ul>
                    <?php
                    wp_list_comments(array(
                        'style' => 'ul',
                        'callback' => 'liting_comments',
                        'short_ping' => true,
                    ));
                    ?>
                </ul><!-- .comment-list -->
            </div>
            <?php
            the_comments_navigation();
            // If comments are closed and there are comments, let's leave a little note, shall we?
            if (!comments_open()) :
                ?>
                <p class="no-comments"><?php esc_html_e('Comments are closed.', 'liting'); ?></p>
                <?php
            endif;
        endif; // Check for have_comments().
        ?>
    </div>
    <?php
//You can start editing here -- including this comment!
endif;

?>
  
            <?php
      
        $user = wp_get_current_user();
        $liting_user_identity = $user->display_name;
        $req = get_option('require_name_email');
        $aria_req = $req ? " aria-required='true'" : '';


    if(is_user_logged_in()){

        $formargs = array(
            'id_form' => 'comment-form',
            'id_submit' => 'post-comment',
            'class_submit' => 'add-btn contact-btn',
            'class_form' => 'form-default',
            'title_reply' => esc_html__('Leave a Reply', 'liting'),
            'title_reply_to' => esc_html__('Leave a Reply to %s', 'liting'),
            'cancel_reply_link' => esc_html__('Cancel Reply', 'liting'),
            'label_submit' => esc_html__('Post Comment', 'liting'),
            'submit_button' => '<button type="submit" name="%1$s" id="%2$s" class="%3$s">%4$s</button>',
            'comment_field' => '<div class="row"><div class="col-lg-12 col-md-12 col-sm-12 comment-form-comment"><textarea placeholder="' . esc_attr__('Your Comment..', 'liting') . '"  class="message-box" id="comment" name="comment" cols="30" rows="10" aria-required="true">' .
            '</textarea></div></div>',
            'must_log_in' => '<div>' .
            sprintf(
                    wp_kses(__('You must be <a href="%s">logged in</a> to post a comment.', 'liting'), array('a' => array('href' => array()))), wp_login_url(apply_filters('the_permalink', esc_url(get_permalink())))
            ) . '</div>',
            'logged_in_as' => '<div class="logged-in-as">' .
            sprintf(
                    wp_kses(__('Logged in as <a href="%1$s">%2$s</a>. <a href="%3$s" title="%4$s">Log out?</a>', 'liting'), array('a' => array('href' => array()))), esc_url(admin_url('profile.php')), $liting_user_identity, wp_logout_url(apply_filters('the_permalink', esc_url(get_permalink()))), esc_attr__('Log out of this account', 'liting')
            ) . '</div>',
            'comment_notes_before' => '<span class="email-notes">' .
            esc_html__('Your email address will not be published.', 'liting') . ( $req ? '<span class="required">*</span>' : '' ) .
            '</span>',
            'comment_notes_after' => '',
            'fields' => apply_filters('comment_form_default_fields', array(
                'author' =>
                '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"><p>' . esc_attr__('Name *', 'liting') . '</p>'
                . '<input id="author" name="author" type="text" value="' . esc_attr($commenter['comment_author']) .
                '" size="30"' . $aria_req . ' /></div>',
                'email' =>
                '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"><p>' . esc_attr__('Email *', 'liting') . '</p>'
                . '<input id="email" name="email" type="text" value="' . esc_attr($commenter['comment_author_email']) .
                '" size="30"' . $aria_req . ' /></div>',
                    )
            )
        );
    }else{

        $formargs = array(
            'id_form' => 'comment-form',
            'id_submit' => 'post-comment',
            'class_submit' => 'add-btn contact-btn',
            'class_form' => 'form-default',
            'title_reply' => esc_html__('Leave a Reply', 'liting'),
            'title_reply_to' => esc_html__('Leave a Reply to %s', 'liting'),
            'cancel_reply_link' => esc_html__('Cancel Reply', 'liting'),
            'label_submit' => esc_html__('Post Comment', 'liting'),
            'submit_button' => '<button type="submit" name="%1$s" id="%2$s" class="%3$s">%4$s</button>',
            'comment_field' => '<div class="row"><div class="col-lg-12 col-md-12 col-sm-12 comment-form-comment"><textarea placeholder="' . esc_attr__('Your Comment..', 'liting') . '"  class="message-box" id="comment" name="comment" cols="30" rows="10" aria-required="true">' .
            '</textarea></div>',
            'must_log_in' => '<div>' .
            sprintf(
                    wp_kses(__('You must be <a href="%s">logged in</a> to post a comment.', 'liting'), array('a' => array('href' => array()))), wp_login_url(apply_filters('the_permalink', esc_url(get_permalink())))
            ) . '</div>',
            'logged_in_as' => '<div class="logged-in-as">' .
            sprintf(
                    wp_kses(__('Logged in as <a href="%1$s">%2$s</a>. <a href="%3$s" title="%4$s">Log out?</a>', 'liting'), array('a' => array('href' => array()))), esc_url(admin_url('profile.php')), $liting_user_identity, wp_logout_url(apply_filters('the_permalink', esc_url(get_permalink()))), esc_attr__('Log out of this account', 'liting')
            ) . '</div>',
            'comment_notes_before' => '<span class="email-notes">' .
            esc_html__('Your email address will not be published.', 'liting') . ( $req ? '<span class="required">*</span>' : '' ) .
            '</span>',
            'comment_notes_after' => '',
            'fields' => apply_filters('comment_form_default_fields', array(
                'author' =>
                '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"><p>' . esc_attr__('Name *', 'liting') . '</p>'
                . '<input id="author" name="author" type="text" value="' . esc_attr($commenter['comment_author']) .
                '" size="30"' . $aria_req . ' /></div>',
                'email' =>
                '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"><p>' . esc_attr__('Email *', 'liting') . '</p>'
                . '<input id="email" name="email" type="text" value="' . esc_attr($commenter['comment_author_email']) .
                '" size="30"' . $aria_req . ' /></div></div>',
                    )
            )
        );
    }

        ?>
        <div class="comment-respond">
        <?php
        comment_form($formargs);
        ?>
        </div>
    <?php 
