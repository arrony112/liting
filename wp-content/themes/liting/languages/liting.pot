#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Liting\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-09-29 22:03+0600\n"
"PO-Revision-Date: 2018-07-18 10:21+0600\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: Enventer <enventer@gmail.com>\n"
"Language: en\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"
"X-Generator: Poedit 2.2.3\n"
"X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;esc_html_e;esc_attr_e;"
"esc_html__\n"
"X-Poedit-Basepath: E:/xampp/htdocs/wp-themes/liting/wp-content/themes/"
"liting\n"
"Last-Translator: \n"
"X-Poedit-SearchPath-0: .\n"

#: 404.php:33
msgid "Error Page"
msgstr ""

#: 404.php:53
msgid "404"
msgstr ""

#: 404.php:54
msgid "Page Not Found"
msgstr ""

#: 404.php:55
msgid "The page you were looking for could not be found. "
msgstr ""

#: 404.php:56
msgid "Go to Home"
msgstr ""

#: archive.php:88 archive.php:106 index.php:92 index.php:110 search.php:92
#: search.php:110
msgid "Read more"
msgstr ""

#: comments.php:32
msgid "1 Comment"
msgstr ""

#: comments.php:65
msgid "Comments are closed."
msgstr ""

#: comments.php:92 comments.php:130
msgid "Leave a Reply"
msgstr ""

#: comments.php:93 comments.php:131
#, php-format
msgid "Leave a Reply to %s"
msgstr ""

#: comments.php:94 comments.php:132
msgid "Cancel Reply"
msgstr ""

#: comments.php:95 comments.php:133
msgid "Post Comment"
msgstr ""

#: comments.php:101 comments.php:139
#, php-format
msgid "You must be <a href=\"%s\">logged in</a> to post a comment."
msgstr ""

#: comments.php:105 comments.php:143
#, php-format
msgid ""
"Logged in as <a href=\"%1$s\">%2$s</a>. <a href=\"%3$s\" title=\"%4$s"
"\">Log out?</a>"
msgstr ""

#: comments.php:108 comments.php:146
msgid "Your email address will not be published."
msgstr ""

#: framework/class-tgm-plugin-activation.php:331 framework/config-tgm.php:81
msgid "Install Required Plugins"
msgstr ""

#: framework/class-tgm-plugin-activation.php:332 framework/config-tgm.php:82
msgid "Install Plugins"
msgstr ""

#: framework/class-tgm-plugin-activation.php:334 framework/config-tgm.php:83
#, php-format
msgid "Installing Plugin: %s"
msgstr ""

#: framework/class-tgm-plugin-activation.php:336
#, php-format
msgid "Updating Plugin: %s"
msgstr ""

#: framework/class-tgm-plugin-activation.php:337 framework/config-tgm.php:84
msgid "Something went wrong with the plugin API."
msgstr ""

#: framework/class-tgm-plugin-activation.php:371 framework/config-tgm.php:95
msgid "Return to Required Plugins Installer"
msgstr ""

#: framework/class-tgm-plugin-activation.php:372
#: framework/class-tgm-plugin-activation.php:833
#: framework/class-tgm-plugin-activation.php:2494
#: framework/class-tgm-plugin-activation.php:3542
msgid "Return to the Dashboard"
msgstr ""

#: framework/class-tgm-plugin-activation.php:373
#: framework/class-tgm-plugin-activation.php:3119 framework/config-tgm.php:96
msgid "Plugin activated successfully."
msgstr ""

#: framework/class-tgm-plugin-activation.php:374
#: framework/class-tgm-plugin-activation.php:2906
msgid "The following plugin was activated successfully:"
msgid_plural "The following plugins were activated successfully:"
msgstr[0] ""
msgstr[1] ""

#: framework/class-tgm-plugin-activation.php:376
#, php-format
msgid "No action taken. Plugin %1$s was already active."
msgstr ""

#: framework/class-tgm-plugin-activation.php:378
#, php-format
msgid ""
"Plugin not activated. A higher version of %s is needed for this theme. "
"Please update the plugin."
msgstr ""

#: framework/class-tgm-plugin-activation.php:380
#, php-format
msgid "All plugins installed and activated successfully. %1$s"
msgstr ""

#: framework/class-tgm-plugin-activation.php:381
msgid "Dismiss this notice"
msgstr ""

#: framework/class-tgm-plugin-activation.php:382
msgid ""
"There are one or more required or recommended plugins to install, update "
"or activate."
msgstr ""

#: framework/class-tgm-plugin-activation.php:383
msgid "Please contact the administrator of this site for help."
msgstr ""

#: framework/class-tgm-plugin-activation.php:569
msgid "Update Required"
msgstr ""

#: framework/class-tgm-plugin-activation.php:929
msgid ""
"The remote plugin package does not contain a folder with the desired slug "
"and renaming did not work."
msgstr ""

#: framework/class-tgm-plugin-activation.php:929
#: framework/class-tgm-plugin-activation.php:932
msgid ""
"Please contact the plugin provider and ask them to package their plugin "
"according to the WordPress guidelines."
msgstr ""

#: framework/class-tgm-plugin-activation.php:932
msgid ""
"The remote plugin package consists of more than one file, but the files "
"are not packaged in a folder."
msgstr ""

#: framework/class-tgm-plugin-activation.php:1955
#, php-format
msgid "TGMPA v%s"
msgstr ""

#: framework/class-tgm-plugin-activation.php:2251
msgid "Required"
msgstr ""

#: framework/class-tgm-plugin-activation.php:2254
msgid "Recommended"
msgstr ""

#: framework/class-tgm-plugin-activation.php:2270
msgid "WordPress Repository"
msgstr ""

#: framework/class-tgm-plugin-activation.php:2273
msgid "External Source"
msgstr ""

#: framework/class-tgm-plugin-activation.php:2276
msgid "Pre-Packaged"
msgstr ""

#: framework/class-tgm-plugin-activation.php:2293
msgid "Not Installed"
msgstr ""

#: framework/class-tgm-plugin-activation.php:2297
msgid "Installed But Not Activated"
msgstr ""

#: framework/class-tgm-plugin-activation.php:2299
msgid "Active"
msgstr ""

#: framework/class-tgm-plugin-activation.php:2305
msgid "Required Update not Available"
msgstr ""

#: framework/class-tgm-plugin-activation.php:2307
msgid "Requires Update"
msgstr ""

#: framework/class-tgm-plugin-activation.php:2309
msgid "Update recommended"
msgstr ""

#: framework/class-tgm-plugin-activation.php:2318
#, php-format
msgctxt "Install/Update Status"
msgid "%1$s, %2$s"
msgstr ""

#: framework/class-tgm-plugin-activation.php:2366
#, php-format
msgid "To Install <span class=\"count\">(%s)</span>"
msgid_plural "To Install <span class=\"count\">(%s)</span>"
msgstr[0] ""
msgstr[1] ""

#: framework/class-tgm-plugin-activation.php:2370
#, php-format
msgid "Update Available <span class=\"count\">(%s)</span>"
msgid_plural "Update Available <span class=\"count\">(%s)</span>"
msgstr[0] ""
msgstr[1] ""

#: framework/class-tgm-plugin-activation.php:2374
#, php-format
msgid "To Activate <span class=\"count\">(%s)</span>"
msgid_plural "To Activate <span class=\"count\">(%s)</span>"
msgstr[0] ""
msgstr[1] ""

#: framework/class-tgm-plugin-activation.php:2448
msgctxt "as in: \"version nr unknown\""
msgid "unknown"
msgstr ""

#: framework/class-tgm-plugin-activation.php:2456
msgid "Installed version:"
msgstr ""

#: framework/class-tgm-plugin-activation.php:2462
msgid "Minimum required version:"
msgstr ""

#: framework/class-tgm-plugin-activation.php:2473
msgid "Available version:"
msgstr ""

#: framework/class-tgm-plugin-activation.php:2494
msgid "No plugins to install, update or activate."
msgstr ""

#: framework/class-tgm-plugin-activation.php:2508
msgid "Plugin"
msgstr ""

#: framework/class-tgm-plugin-activation.php:2509
msgid "Source"
msgstr ""

#: framework/class-tgm-plugin-activation.php:2510
msgid "Type"
msgstr ""

#: framework/class-tgm-plugin-activation.php:2514
msgid "Version"
msgstr ""

#: framework/class-tgm-plugin-activation.php:2515
msgid "Status"
msgstr ""

#: framework/class-tgm-plugin-activation.php:2564
#, php-format
msgid "Install %2$s"
msgstr ""

#: framework/class-tgm-plugin-activation.php:2569
#, php-format
msgid "Update %2$s"
msgstr ""

#: framework/class-tgm-plugin-activation.php:2575
#, php-format
msgid "Activate %2$s"
msgstr ""

#: framework/class-tgm-plugin-activation.php:2641
msgid "Upgrade message from the plugin author:"
msgstr ""

#: framework/class-tgm-plugin-activation.php:2674
msgid "Install"
msgstr ""

#: framework/class-tgm-plugin-activation.php:2680
msgid "Update"
msgstr ""

#: framework/class-tgm-plugin-activation.php:2683
msgid "Activate"
msgstr ""

#: framework/class-tgm-plugin-activation.php:2714
msgid "No plugins were selected to be installed. No action taken."
msgstr ""

#: framework/class-tgm-plugin-activation.php:2716
msgid "No plugins were selected to be updated. No action taken."
msgstr ""

#: framework/class-tgm-plugin-activation.php:2757
msgid "No plugins are available to be installed at this time."
msgstr ""

#: framework/class-tgm-plugin-activation.php:2759
msgid "No plugins are available to be updated at this time."
msgstr ""

#: framework/class-tgm-plugin-activation.php:2863
msgid "No plugins were selected to be activated. No action taken."
msgstr ""

#: framework/class-tgm-plugin-activation.php:2889
msgid "No plugins are available to be activated at this time."
msgstr ""

#: framework/class-tgm-plugin-activation.php:3118
msgid "Plugin activation failed."
msgstr ""

#: framework/class-tgm-plugin-activation.php:3461
#, php-format
msgid "Updating Plugin %1$s (%2$d/%3$d)"
msgstr ""

#: framework/class-tgm-plugin-activation.php:3464
#, php-format
msgid "An error occurred while installing %1$s: <strong>%2$s</strong>."
msgstr ""

#: framework/class-tgm-plugin-activation.php:3466
#, php-format
msgid "The installation of %1$s failed."
msgstr ""

#: framework/class-tgm-plugin-activation.php:3470
msgid ""
"The installation and activation process is starting. This process may take "
"a while on some hosts, so please be patient."
msgstr ""

#: framework/class-tgm-plugin-activation.php:3472
#, php-format
msgid "%1$s installed and activated successfully."
msgstr ""

#: framework/class-tgm-plugin-activation.php:3472
#: framework/class-tgm-plugin-activation.php:3480
msgid "Show Details"
msgstr ""

#: framework/class-tgm-plugin-activation.php:3472
#: framework/class-tgm-plugin-activation.php:3480
msgid "Hide Details"
msgstr ""

#: framework/class-tgm-plugin-activation.php:3473
msgid "All installations and activations have been completed."
msgstr ""

#: framework/class-tgm-plugin-activation.php:3475
#, php-format
msgid "Installing and Activating Plugin %1$s (%2$d/%3$d)"
msgstr ""

#: framework/class-tgm-plugin-activation.php:3478
msgid ""
"The installation process is starting. This process may take a while on "
"some hosts, so please be patient."
msgstr ""

#: framework/class-tgm-plugin-activation.php:3480
#, php-format
msgid "%1$s installed successfully."
msgstr ""

#: framework/class-tgm-plugin-activation.php:3481
msgid "All installations have been completed."
msgstr ""

#: framework/class-tgm-plugin-activation.php:3483
#, php-format
msgid "Installing Plugin %1$s (%2$d/%3$d)"
msgstr ""

#: framework/config-tgm.php:13
msgid "liting Core"
msgstr ""

#: framework/config-tgm.php:21
msgid "liting Demo Installer"
msgstr ""

#: framework/config-tgm.php:29
msgid "Redux Framework"
msgstr ""

#: framework/config-tgm.php:37
msgid "Contact Form 7"
msgstr ""

#: framework/config-tgm.php:45
msgid "Elementor"
msgstr ""

#: framework/config-tgm.php:53
msgid "Meta Box"
msgstr ""

#: framework/config-tgm.php:61
msgid "MailChimp for WordPress"
msgstr ""

#: framework/config-tgm.php:97
#, php-format
msgid "All plugins installed and activated successfully. %s"
msgstr ""

#: framework/redux.config.php:33 framework/redux.config.php:34
msgid "liting Options"
msgstr ""

#: framework/redux.config.php:131
msgid "Header Settings"
msgstr ""

#: framework/redux.config.php:133 framework/redux.config.php:155
#: framework/redux.config.php:238
msgid "These are really basic fields!"
msgstr ""

#: framework/redux.config.php:140
msgid "Header Button Text"
msgstr ""

#: framework/redux.config.php:141
msgid "Sign in"
msgstr ""

#: framework/redux.config.php:146
msgid "Header Button URL"
msgstr ""

#: framework/redux.config.php:153
msgid "Blog Settings"
msgstr ""

#: framework/redux.config.php:162
msgid "Blog Title"
msgstr ""

#: framework/redux.config.php:167
msgid "Banner"
msgstr ""

#: framework/redux.config.php:168 framework/redux.config.php:246
msgid "Enable or Disable"
msgstr ""

#: framework/redux.config.php:170 framework/redux.config.php:248
msgid "Enable"
msgstr ""

#: framework/redux.config.php:171 framework/redux.config.php:249
msgid "Disable"
msgstr ""

#: framework/redux.config.php:178 framework/redux.config.php:262
msgid "Basic media uploader with disabled URL input field."
msgstr ""

#: framework/redux.config.php:179 framework/redux.config.php:263
msgid "Add/Upload Header Image using the WordPress native uploader"
msgstr ""

#: framework/redux.config.php:180
msgid "Header Image"
msgstr ""

#: framework/redux.config.php:185
msgid "Post Style"
msgstr ""

#: framework/redux.config.php:186
msgid "Blog style List or Grid"
msgstr ""

#: framework/redux.config.php:189
msgid "List"
msgstr ""

#: framework/redux.config.php:190
msgid "Grid"
msgstr ""

#: framework/redux.config.php:198
msgid "Typography"
msgstr ""

#: framework/redux.config.php:200
msgid "Theme all font options"
msgstr ""

#: framework/redux.config.php:207
msgid "Body Typography"
msgstr ""

#: framework/redux.config.php:208
msgid "Select body font family, size, line height, color and weight."
msgstr ""

#: framework/redux.config.php:222
msgid "Page Title"
msgstr ""

#: framework/redux.config.php:223
msgid "Page title Typography Settings"
msgstr ""

#: framework/redux.config.php:236
msgid "Footer Settings"
msgstr ""

#: framework/redux.config.php:245
msgid "Footer Widget"
msgstr ""

#: framework/redux.config.php:254
msgid "Copyright Text"
msgstr ""

#: framework/redux.config.php:264
msgid "Footer Background"
msgstr ""

#: functions.php:62
msgid "Primary"
msgstr ""

#: functions.php:107
msgid "Small"
msgstr ""

#: functions.php:108
msgid "S"
msgstr ""

#: functions.php:113
msgid "Normal"
msgstr ""

#: functions.php:114
msgid "M"
msgstr ""

#: functions.php:119
msgid "Large"
msgstr ""

#: functions.php:120
msgid "L"
msgstr ""

#: functions.php:125
msgid "Huge"
msgstr ""

#: functions.php:126
msgid "XL"
msgstr ""

#: functions.php:136
msgid "strong yellow"
msgstr ""

#: functions.php:141
msgid "strong white"
msgstr ""

#: functions.php:146
msgid "light black"
msgstr ""

#: functions.php:151
msgid "very light gray"
msgstr ""

#: functions.php:156
msgid "very dark black"
msgstr ""

#: functions.php:242
msgid "Sidebar"
msgstr ""

#: functions.php:244
msgid "Add widgets here."
msgstr ""

#: functions.php:257
msgid "Footer Column 1"
msgstr ""

#: functions.php:259 functions.php:270 functions.php:281
msgid "Widgets in this area will be shown on footer widget."
msgstr ""

#: functions.php:268
msgid "Footer Column 2"
msgstr ""

#: functions.php:279
msgid "Footer Column 3"
msgstr ""

#: functions.php:319
msgid ", "
msgstr ""

#: functions.php:352
msgid "Post author"
msgstr ""

#: functions.php:356
msgid "Reply "
msgstr ""

#: page.php:62 single.php:100 template-parts/content.php:40
msgid "Pages:"
msgstr ""

#: search.php:37
#, php-format
msgid "Search Results for: %s"
msgstr ""

#: search.php:39 template-parts/content-none.php:14
msgid "Nothing Found"
msgstr ""

#: searchform.php:14
msgid "Search..."
msgstr ""

#: template-parts/content-none.php:24
#, php-format
msgid ""
"Ready to publish your first post? <a href=\"%1$s\">Get started here</a>."
msgstr ""

#: template-parts/content-none.php:37
msgid ""
"Sorry, but nothing matched your search terms. Please try again with some "
"different keywords."
msgstr ""

#: template-parts/content-none.php:47
msgid ""
"It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps "
"searching can help."
msgstr ""

#: template-parts/content.php:44
msgid "Read More"
msgstr ""
