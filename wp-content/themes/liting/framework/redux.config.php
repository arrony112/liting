<?php

/**
 * ReduxFramework Barebones Sample Config File
 * For full documentation, please visit: http://docs.reduxframework.com/
 */
if (!class_exists('Redux')) {
    return;
}

// This is your option name where all the Redux data is stored.
$opt_name = "liting_options";
$opt_prefix = 'liting';
/**
 * ---> SET ARGUMENTS
 * All the possible arguments for Redux.
 * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
 * */
$theme = wp_get_theme(); // For use with some settings. Not necessary.

$args = array(
    // TYPICAL -> Change these values as you need/desire
    'opt_name' => $opt_name,
    // This is where your data is stored in the database and also becomes your global variable name.
    'display_name' => $theme->get('Name'),
    // Name that appears at the top of your panel
    'display_version' => $theme->get('Version'),
    // Version that appears at the top of your panel
    'menu_type' => 'menu',
    //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
    'allow_sub_menu' => true,
    // Show the sections below the admin menu item or not
    'menu_title' => esc_html__('liting Options', 'liting'),
    'page_title' => esc_html__('liting Options', 'liting'),
    // You will need to generate a Google API key to use this feature.
    // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
    'google_api_key' => '',
    // Set it you want google fonts to update weekly. A google_api_key value is required.
    'google_update_weekly' => false,
    // Must be defined to add google fonts to the typography module
    'async_typography' => true,
    // Use a asynchronous font on the front end or font string
    //'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
    'admin_bar' => true,
    // Show the panel pages on the admin bar
    'admin_bar_icon' => 'dashicons-portfolio',
    // Choose an icon for the admin bar menu
    'admin_bar_priority' => 50,
    // Choose an priority for the admin bar menu
    'global_variable' => '',
    // Set a different name for your global variable other than the opt_name
    'dev_mode' => true,
    // Show the time the page took to load, etc
    'update_notice' => true,
    // If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
    'customizer' => true,
    // Enable basic customizer support
    //'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
    //'disable_save_warn' => true,                    // Disable the save warning when a user changes a field
    // OPTIONAL -> Give you extra features
    'page_priority' => null,
    // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
    'page_parent' => 'themes.php',
    // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
    'page_permissions' => 'manage_options',
    // Permissions needed to access the options panel.
    'menu_icon' => '',
    // Specify a custom URL to an icon
    'last_tab' => '',
    // Force your panel to always open to a specific tab (by id)
    'page_icon' => 'icon-themes',
    // Icon displayed in the admin panel next to your menu_title
    'page_slug' => '_options',
    // Page slug used to denote the panel
    'save_defaults' => true,
    // On load save the defaults to DB before user clicks save or not
    'default_show' => false,
    // If true, shows the default value next to each field that is not the default value.
    'default_mark' => '',
    // What to print by the field's title if the value shown is default. Suggested: *
    'show_import_export' => true,
    // Shows the Import/Export panel when not used as a field.
    // CAREFUL -> These options are for advanced use only
    'transient_time' => 60 * MINUTE_IN_SECONDS,
    'output' => true,
    // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
    'output_tag' => true,
    // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
    // 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.
    // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
    'database' => '',
    // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
    'use_cdn' => true,
    // If you prefer not to use the CDN for Select2, Ace Editor, and others, you may download the Redux Vendor Support plugin yourself and run locally or embed it in your code.
    //'compiler'             => true,
    // HINTS
    'hints' => array(
        'icon' => 'el el-question-sign',
        'icon_position' => 'right',
        'icon_color' => 'lightgray',
        'icon_size' => 'normal',
        'tip_style' => array(
            'color' => 'light',
            'shadow' => true,
            'rounded' => false,
            'style' => '',
        ),
        'tip_position' => array(
            'my' => 'top left',
            'at' => 'bottom right',
        ),
        'tip_effect' => array(
            'show' => array(
                'effect' => 'slide',
                'duration' => '500',
                'event' => 'mouseover',
            ),
            'hide' => array(
                'effect' => 'slide',
                'duration' => '500',
                'event' => 'click mouseleave',
            ),
        ),
    )
);


Redux::setArgs($opt_name, $args);

Redux::setSection($opt_name, array(
    'title' => esc_html__('Header Settings', 'liting'),
    'id' => 'header_settings',
    'desc' => esc_html__('These are really basic fields!', 'liting'),
    'customizer_width' => '400px',
    'icon' => 'el el-th-large',
    'fields' => array(
        array(
            'id' => $opt_prefix . '_header_button_text',
            'type' => 'text',
            'title' => esc_html__('Header Button Text', 'liting'),
            'default' => esc_html__('Sign in','liting'),
        ),
        array(
            'id' => $opt_prefix . '_header_button_url',
            'type' => 'text',
            'title' => esc_html__('Header Button URL', 'liting'),
        ),

    )
));

Redux::setSection($opt_name, array(
    'title' => esc_html__('Blog Settings', 'liting'),
    'id' => 'blog_settings',
    'desc' => esc_html__('These are really basic fields!', 'liting'),
    'customizer_width' => '400px',
    'icon' => 'el el-th-large',
    'fields' => array(
        array(
            'id' => $opt_prefix . '_blog_title',
            'type' => 'text',
            'title' => esc_html__('Blog Title', 'liting'),
        ),
        array(
            'id' => $opt_prefix . '_blog_banner',
            'type' => 'switch',
            'title' => esc_html__('Banner', 'liting'),
            'subtitle' => esc_html__('Enable or Disable', 'liting'),
            'default' => FALSE,
            'on' => esc_html__('Enable', 'liting'),
            'off' => esc_html__('Disable', 'liting'),
        ),
        array(
            'id' => $opt_prefix . '_blog_header_image',
            'type' => 'media',
            'url' => true,
            'compiler' => 'true',
            'desc' => esc_html__('Basic media uploader with disabled URL input field.', 'liting'),
            'subtitle' => esc_html__('Add/Upload Header Image using the WordPress native uploader', 'liting'),
            'title' => esc_html__('Header Image', 'liting')
        ),
        array(
            'id' => $opt_prefix . '_blog_style',
            'type' => 'select',
            'title' => esc_html__('Post Style', 'liting'),
            'subtitle' => esc_html__('Blog style List or Grid', 'liting'),
            'default' => '1',
            'options' => array(
                '1' => esc_html__('List','liting'),
                '2' => esc_html__('Grid','liting'),
            )
        ),

    )
));

Redux::setSection($opt_name, array(
    'title' => esc_html__('Typography', 'liting'),
    'id' => 'typography',
    'desc' => esc_html__('Theme all font options', 'liting'),
    'customizer_width' => '400px',
    'icon' => 'el el-font',
    'fields' => array(
        array(
            'id' => $opt_prefix . 'body_typography',
            'type' => 'typography',
            'title' => esc_html__('Body Typography', 'liting'),
            'subtitle' => esc_html__('Select body font family, size, line height, color and weight.', 'liting'),
            'text-align' => false,
            'subsets' => false,
            'default' => array(
                'color' => '#888e94',
                'font-weight' => '400',
                'font-family' => 'Poppins',
                'google' => true,
                'font-size' => '15px',
            ),
        ),
        array(
            'id' => $opt_prefix . 'page_title_typography',
            'type' => 'typography',
            'title' => esc_html__('Page Title', 'liting'),
            'subtitle' => esc_html__('Page title Typography Settings', 'liting'),
            'text-align' => false,
            'line-height' => false,
            'subsets' => false,
            'color' => false,
            'font-size' => false,
            'output' => array('h1', 'h2', 'h3', 'h4', 'h5', 'h6'),
        ),
    )
));


Redux::setSection($opt_name, array(
    'title' => esc_html__('Footer Settings', 'liting'),
    'id' => 'footer_settings',
    'desc' => esc_html__('These are really basic fields!', 'liting'),
    'customizer_width' => '400px',
    'icon' => 'el el-th-large',
    'fields' => array(
        array(
            'id' => $opt_prefix . '_footer_widget',
            'type' => 'switch',
            'title' => esc_html__('Footer Widget', 'liting'),
            'subtitle' => esc_html__('Enable or Disable', 'liting'),
            'default' => false,
            'on' => esc_html__('Enable', 'liting'),
            'off' => esc_html__('Disable', 'liting'),
        ),
        array(
            'id' => $opt_prefix . '_copyright_text',
            'type' => 'textarea',
            'title' => esc_html__('Copyright Text', 'liting'),
            'default' => wp_kses_post('© 2019 All Right Reserved By <a href="#">liting</a>','liting'),
        ),
        array(
            'id' => $opt_prefix . '_footer_bg',
            'type' => 'media',
            'url' => true,
            'compiler' => 'true',
            'desc' => esc_html__('Basic media uploader with disabled URL input field.', 'liting'),
            'subtitle' => esc_html__('Add/Upload Header Image using the WordPress native uploader', 'liting'),
            'title' => esc_html__('Footer Background', 'liting')
        ),
    )
));