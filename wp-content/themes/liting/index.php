<?php 
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package liting
 */
get_header();

global $liting_options;
$blog_header_url = isset($liting_options['liting_blog_header_image']['url']) ? $liting_options['liting_blog_header_image']['url'] : '';
$blog_title = isset($liting_options['liting_blog_title']) ? $liting_options['liting_blog_title'] : '';
$blog_banner = isset($liting_options['liting_blog_banner']) ? $liting_options['liting_blog_banner'] : '0';

$liting_blog_style = get_query_var('blog_type');

if (!$liting_blog_style) {
    $liting_blog_style = $liting_options['liting_blog_style'];
}

if($blog_banner == "1"){
?>
    <div class="page-area image_background" data-image-src="<?php echo esc_url($blog_header_url);?>">
        <div class="breadcumb-overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="breadcrumb text-center">
                        <div class="section-headline white-headline text-center">
                            <?php if(!empty($blog_title)){?>
                            <h3><?php echo wp_kses_post($blog_title);?></h3>
                            <?php }else{ ?>
                                <h3><?php the_title();?></h3>
                            <?php } ?>
                        </div>
                        <?php
                        if (function_exists('bcn_display')) {
                        ?>
                        <ul>
                          <?php  bcn_display();?>
                        </ul>
                       <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
    <div class="blog-area fix area-padding">
            <div class="container">
                <div class="row">
               <?php if($liting_blog_style =='2'){ ?>
                        <div class="blog-grid blog-1">
               <?php }else{ ?>
                  <div class="blog-sidebar-right">
                      <?php if (is_active_sidebar('sidebar')) { ?>
						<div class="col-md-8 col-sm-8 col-xs-12">
                      <?php }else{ ?>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                      <?php } ?>
							<div class="row">
							    <div class="blog-left-content">
                      <?php } ?>
                                <?php
                                if (have_posts()) :
                                    /* Start the Loop */
                                    $i = 1;
                                    while (have_posts()) :
                                        $i++;
                                        $i = $i+1;
                                        the_post();
                                        ?>
                                        <!-- Start single blog -->
                                        <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                                        <?php if($liting_blog_style =='2'){ ?>
                                        <div class="col-md-4 col-sm-6 col-xs-12">
                                            <div class="single-blog wow fadeInUp" data-wow-delay="0.<?php echo esc_attr($i);?>s">
                                                <div class="blog-image">
                                                    <a class="image-scale" href="<?php esc_url(the_permalink()); ?>">
                                                    <?php the_post_thumbnail();?>
                                                    </a>
                                                </div>
                                                <div class="blog-content">
                                                    <?php liting_post_meta();?>
                                                    <a href="<?php esc_url(the_permalink()); ?>">
                                                        <h4><?php the_title(); ?></h4>
                                                    </a>
                                                    <?php the_excerpt(); ?>
                                                    <a class="blog-btn" href="<?php esc_url(the_permalink()); ?>"><?php esc_html_e('Read more','liting');?></a>
                                                </div>
                                            </div>
                                        </div>
                                      <?php  }else{?>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="single-blog wow fadeInUp" data-wow-delay="0.<?php echo esc_attr($i);?>s">
                                                <div class="blog-image">
                                                    <a class="image-scale" href="<?php esc_url(the_permalink()); ?>">
                                                    <?php the_post_thumbnail();?>
                                                    </a>
                                                </div>
                                                <div class="blog-content">
                                                    <?php liting_post_meta();?>
                                                    <a href="<?php esc_url(the_permalink()); ?>">
                                                        <h4><?php the_title(); ?></h4>
                                                    </a>
                                                   <?php the_excerpt(); ?>
                                                    <a class="blog-btn" href="<?php esc_url(the_permalink()); ?>"><?php esc_html_e('Read more','liting');?></a>
                                                </div>
                                            </div>
                                        </div>
                                        <?php } ?>
                                        </div>
                                        <!-- End single blog --> 
                                    <?php
                                    endwhile;
                                else :
                                    get_template_part('template-parts/content', 'none');
                                endif;
                                ?>                                     
                            
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="blog-pagination">
                                            <?php
                                            the_posts_pagination(array(
                                                    'mid_size' => 2,
                                                    'prev_text' => 'Prev',
                                                    'next_text' => 'Next'
                                                ));
                                            ?>
                                        </div>	
                        <?php if($liting_blog_style =='2'){ ?>
                            </div>
                       <?php }else{?>
                                    </div>
                                </div>
                            </div>
                        </div>
                       <?php } ?>
                        <!-- Start Right Sidebar blog -->
                        <?php if($liting_blog_style !='2'){ ?>
                         <?php if (is_active_sidebar('sidebar')) { ?>
						<div class="col-md-4 col-sm-4 col-xs-12">
							<div class="left-head-blog right-side">
                                <?php
                                    get_sidebar('sidebar');
                                ?>
							</div>
                        </div>
                       <?php } }?>
						<!-- End Right Sidebar -->
                    </div>
                </div>
                <!-- End row -->
            </div>
        </div>    

<?php get_footer()?>