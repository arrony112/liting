<?php
/**
 * The template for displaying search results pages
 *
 *
 * @package Liting
 */

get_header(); ?>
<?php $unique_id = esc_attr( uniqid( 'search-form-' ) ); ?>

<form role="search" method="get"  action="<?php echo esc_url( home_url( '/' ) ); ?>">
<div class="blog-search-option">
    <input type="search" placeholder="<?php esc_attr_e( 'Search...','liting' ); ?>" value="<?php echo get_search_query(); ?>" name="s"  required="">
    <button type="submit" class="button"><i class="fa fa-search"></i></button>
    </div>
</form>
