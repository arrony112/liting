<?php
/**
 * liting functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package liting
 */
defined('LITING_THEME_URI') or define('LITING_THEME_URI', get_template_directory_uri());
define('LITING_CSS_URL', LITING_THEME_URI . '/assets/css');
define('LITING_JS_URL', LITING_THEME_URI . '/assets/js');
define('LITING_FONTS_URL', LITING_THEME_URI . '/assets/fonts');
define('LITING_IMG_URL', LITING_THEME_URI . '/assets/images/');
define('LITING_THEME_DIR', get_template_directory());
define('LITING_FREAMWORK_DIRECTORY', LITING_THEME_DIR . '/framework/');
define('LITING_INC_DIRECTORY', LITING_THEME_DIR . '/inc/');

/** Redux framework configuration */
require_once(LITING_FREAMWORK_DIRECTORY . "redux.config.php");

require_once( LITING_INC_DIRECTORY . "template-tags.php" );

/** Enable support TGM features. */
require_once(LITING_FREAMWORK_DIRECTORY . "class-tgm-plugin-activation.php");
require_once(LITING_FREAMWORK_DIRECTORY . "config-tgm.php");

if (!function_exists('liting_setup')) :

    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     */
    function liting_setup() {
        /*
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         * If you're building a theme based on liting, use a find and replace
         * to change 'liting' to the name of your theme in all the template files.
         */
        load_theme_textdomain('liting', get_template_directory() . '/languages');
        add_editor_style(LITING_CSS_URL . '/editor-style.css');
        // Add default posts and comments RSS feed links to head.
        add_theme_support('automatic-feed-links');
        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support('title-tag');
        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
         */
        add_theme_support('post-thumbnails');
        // This theme uses wp_nav_menu() in one location.~
        register_nav_menus(array(
            'primary' => esc_html__('Primary', 'liting'),
        ));

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support('html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ));
        // Set up the WordPress core custom background feature.
        add_theme_support('custom-background', apply_filters('liting_custom_background_args', array(
            'default-color' => 'ffffff',
            'default-image' => '',
        )));
        // Add theme support for selective refresh for widgets.
        add_theme_support('customize-selective-refresh-widgets');
        /**
         * Add support for core custom logo.
         *
         * @link https://codex.wordpress.org/Theme_Logo
         */
        add_theme_support('custom-logo', array(
            'height' => 250,
            'width' => 250,
            'flex-width' => true,
            'flex-height' => true,
        ));

        // Add theme support for selective refresh for widgets.
        add_theme_support('customize-selective-refresh-widgets');
        // Add support for Block Styles.
        add_theme_support('wp-block-styles');
        // Add support for full and wide align images.
        add_theme_support('align-wide');
        // Add support for editor styles.
        add_theme_support('editor-styles');
        // Add custom editor font sizes.
        add_theme_support(
                'editor-font-sizes', array(
            array(
                'name' => esc_html__('Small', 'liting'),
                'shortName' => esc_html__('S', 'liting'),
                'size' => 19.5,
                'slug' => 'small',
            ),
            array(
                'name' => esc_html__('Normal', 'liting'),
                'shortName' => esc_html__('M', 'liting'),
                'size' => 22,
                'slug' => 'normal',
            ),
            array(
                'name' => esc_html__('Large', 'liting'),
                'shortName' => esc_html__('L', 'liting'),
                'size' => 36.5,
                'slug' => 'large',
            ),
            array(
                'name' => esc_html__('Huge', 'liting'),
                'shortName' => esc_html__('XL', 'liting'),
                'size' => 49.5,
                'slug' => 'huge',
            ),
                )
        );

// Editor color palette.
        add_theme_support('editor-color-palette', array(
            array(
                'name' => esc_html__('strong yellow', 'liting'),
                'slug' => 'strong-yellow',
                'color' => '#f7bd00',
            ),
            array(
                'name' => esc_html__('strong white', 'liting'),
                'slug' => 'strong-white',
                'color' => '#fff',
            ),
            array(
                'name' => esc_html__('light black', 'liting'),
                'slug' => 'light-black',
                'color' => '#242424',
            ),
            array(
                'name' => esc_html__('very light gray', 'liting'),
                'slug' => 'very-light-gray',
                'color' => '#797979',
            ),
            array(
                'name' => esc_html__('very dark black', 'liting'),
                'slug' => 'very-dark-black',
                'color' => '#000000',
            ),
        ));
        // Add support for responsive embedded content.
        add_theme_support('responsive-embeds');
        //Add custom thumb size
        set_post_thumbnail_size('full', false);
        add_image_size('liting_blog_post_featured_image', 770, 462, true);
        add_image_size('liting_sidebar_post_image', 75, 70, true);
        add_image_size('liting-serives-single', 770, 441, true);
        add_image_size('liting-blog-carousel', 370, 411, true);
        add_image_size('liting-blog-sidebar', 75, 70, true);
    }

endif;
add_action('after_setup_theme', 'liting_setup');







function liting_scripts() {
    wp_enqueue_style('bootstrap-css', LITING_CSS_URL . '/bootstrap.min.css', '', null);
    wp_enqueue_style('owl-carousel', LITING_CSS_URL . '/owl.carousel.css', '', null);
    wp_enqueue_style('owl-transitions', LITING_CSS_URL . '/owl.transitions.css', '', null);
    wp_enqueue_style('animate-css', LITING_CSS_URL . '/animate.css', array('elementor-animations'), null);
    wp_enqueue_style('meanmenu-css', LITING_CSS_URL . '/meanmenu.min.css', '', null);
    wp_enqueue_style('font-awesome', LITING_CSS_URL . '/font-awesome.min.css', '', null);
    wp_enqueue_style('icon-css', LITING_CSS_URL . '/icon.css', '', null);
    wp_enqueue_style('flaticon', LITING_CSS_URL . '/flaticon.css', '', null);
    wp_enqueue_style('magnific', LITING_CSS_URL . '/magnific.min.css', '', null);
    wp_enqueue_style('liting-style', get_stylesheet_uri());
    wp_enqueue_style('responsive', LITING_CSS_URL . '/responsive.css', '', null);

    wp_enqueue_script('jquery-ui-core');
    wp_enqueue_script('bootstrap', LITING_JS_URL . '/bootstrap.min.js', array('jquery'), '', true);
    wp_enqueue_script('owl-carousel-js', LITING_JS_URL . '/owl.carousel.min.js', array('jquery'), '', true);
    wp_enqueue_script('jquery-counterup', LITING_JS_URL . '/jquery.counterup.min.js', array('jquery'), '', true);
    wp_enqueue_script('waypoints', LITING_JS_URL . '/waypoints.js', array('jquery'), '', true);
    wp_enqueue_script('magnific-js', LITING_JS_URL . '/magnific.min.js', array('jquery'), '', true);
    wp_enqueue_script('wow-js', LITING_JS_URL . '/wow.min.js', array('jquery'), '', true);
    wp_enqueue_script('jquery-meanmenu', LITING_JS_URL . '/jquery.meanmenu.js', array('jquery'), '', true);
    wp_enqueue_script('plugins-js', LITING_JS_URL . '/plugins.js', array('jquery'), '', true);
    wp_enqueue_script('liting-main', LITING_JS_URL . '/main.js', array('jquery'), '', true);


    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }


}

add_action('wp_enqueue_scripts', 'liting_scripts');


/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function liting_content_width() {
// This variable is intended to be overruled from themes.
// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
    $GLOBALS['content_width'] = apply_filters('liting_content_width', 240);
}

add_action('after_setup_theme', 'liting_content_width', 0);



/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function liting_widgets_init() {

    register_sidebar(array(
        'name' => esc_html__('Sidebar', 'liting'),
        'id' => 'sidebar',
        'description' => esc_html__('Add widgets here.', 'liting'),
        'before_widget' => '<div id="%1$s" class="left-blog-page %2$s"><div class="left-blog">',
        'after_widget' => '</div></div>',
        'before_title' => '<h4>',
        'after_title' => '</h4>',
    ));

    global $liting_options;
    $liting_footer_widget = isset($liting_options['liting_footer_widget']) ? $liting_options['liting_footer_widget'] : 0;
    if($liting_footer_widget == 1){

        register_sidebar(
                array(
                    'name' => esc_html__('Footer Column 1', 'liting'),
                    'id' => 'footer_column_1',
                    'description' => esc_html__('Widgets in this area will be shown on footer widget.', 'liting'),
                    'before_widget' => '<div id="%1$s" class="widget footer-content %2$s"><div class="footer-head">',
                    'after_widget' => '</div></div>',
                    'before_title' => '<h4>',
                    'after_title' => '</h4>',
        ));

        register_sidebar(
                array(
                    'name' => esc_html__('Footer Column 2', 'liting'),
                    'id' => 'footer_column_2',
                    'description' => esc_html__('Widgets in this area will be shown on footer widget.', 'liting'),
                    'before_widget' => '<div id="%1$s" class="widget footer-content %2$s"><div class="footer-head">',
                    'after_widget' => '</div></div>',
                    'before_title' => '<h4>',
                    'after_title' => '</h4>',
        ));

        register_sidebar(
                array(
                    'name' => esc_html__('Footer Column 3', 'liting'),
                    'id' => 'footer_column_3',
                    'description' => esc_html__('Widgets in this area will be shown on footer widget.', 'liting'),
                    'before_widget' => '<div id="%1$s" class="widget footer-content last-content %2$s"><div class="footer-head">',
                    'after_widget' => '</div></div>',
                    'before_title' => '<h4>',
                    'after_title' => '</h4>',
        ));

    }
}

add_action('widgets_init', 'liting_widgets_init');

/**
 * google font compatibility.
 */
function liting_google_font() {
    $protocol = is_ssl() ? 'https' : 'http';
    $subsets = 'latin,cyrillic-ext,latin-ext,cyrillic,greek-ext,greek,vietnamese';
    $variants = ':400,400i,500,500i,600,600i,700,700i,800,800i';
    $query_args = array(
        'family' => 'Poppins' . $variants,
        'subset' => $subsets,
    );
    $font_url = add_query_arg($query_args, $protocol . "://fonts.googleapis.com/css");
    wp_enqueue_style('liting-google-fonts', $font_url, array(), NULL);
}
add_action('init', 'liting_google_font');


if (!function_exists('liting_categories')) :

    /**
     * Prints HTML with meta information for the categories, tags and comments.
     */
    function liting_categories() {
        // Hide category and tag text for pages.
        if ('post' === get_post_type()) {
            /* translators: used between list items, there is a space after the comma */
            $categories_list = get_the_category_list(esc_html__(', ', 'liting'));
            if ($categories_list) {
                /* translators: 1: list of categories. */
                printf('<span>' . esc_html('%1$s') . '</span>', $categories_list); // WPCS: XSS OK.
            }
        }
    }

endif;

function liting_add_query_vars_filter($vars) {
    $vars[] = "blog_type";
    return $vars;
}

add_filter('query_vars', 'liting_add_query_vars_filter');


// liting_comments
function liting_comments($comment, $args, $depth) {

        $tag = 'li';
    ?>
    <<?php echo esc_html($tag); ?> <?php comment_class(empty($args['has_children']) ? ' ' : 'parent' ); ?> id="comment-<?php comment_ID() ?>">
        <div id="div-comment-<?php comment_ID() ?>" class="comments-details"><?php
    ?>
            <?php if($comment->comment_type!='trackback' && $comment->comment_type!='pingback' ){ ?>
            <div class="comments-list-img">
                <?php print get_avatar($comment, 70, null, null, array('class' => array())); ?>
            </div>
            <?php } ?>
            <div class="comments-content-wrap">
                <span>
                    <b><?php echo get_comment_author_link(); ?></b><?php esc_html_e('Post author','liting');?>
                    <span class="post-time"><?php comment_time(get_option('date_format')); ?></span>
                    <?php
                    comment_reply_link(array_merge($args, array(
                        'reply_text' => esc_html__('Reply ', 'liting'),
                        'depth' => $depth,
                        'max_depth' => $args['max_depth']
                                    )
                    ));
                    ?>
                </span>
                <?php comment_text(); ?>
            </div>


        <?php
}


