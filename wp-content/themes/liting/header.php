
<?php 
global $liting_options;
$header_button = isset($liting_options['liting_header_button']) ? $liting_options['liting_header_button'] : '';
$header_button_text = isset($liting_options['liting_header_button_text']) ? $liting_options['liting_header_button_text'] : '';
$header_button_url = isset($liting_options['liting_header_button_url']) ? $liting_options['liting_header_button_url'] : '';

$color_select =  get_post_meta(get_the_id(), 'liting_color_select',true); 

$title_breadcumb =  get_post_meta(get_the_id(), 'liting_title_breadcumb',true); 
$header_class = '';
if($title_breadcumb != 'off'){
    $header_class = 'pages-header';
}

$color_class = '';
if($color_select == "red"){
    $color_class = 'home-color-red';
}elseif($color_select == "green"){
    $color_class = 'home-color-green';
}

/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Liting
 */
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <!-- Required meta tags -->
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name=author content="Enventer">
    <?php
    if (function_exists('has_site_icon') && has_site_icon()) { // since 4.3.0
        wp_site_icon();
    }
    ?>
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div class="wappper <?php echo esc_attr($color_class);?>">
    <div id="preloader"></div>
        <header class="header-one">
            <!-- header-area start -->
            <div id="sticker" class="header-area <?php echo esc_attr($header_class);?> hidden-xs">
                <div class="container">
                    <div class="row">
                        <!-- logo start -->
                        <div class="col-md-3 col-sm-3">
                            <div class="logo">
                                <?php if (function_exists('get_custom_logo') && has_custom_logo()) {
                                    the_custom_logo();
                                }else{ ?>
                                <a class="navbar-brand page-scroll black-logo" href="<?php echo esc_url(home_url('/')); ?>">
                                    <img src="<?php echo esc_url(LITING_IMG_URL . 'logo.png') ?>" alt="logo">
                                </a>
                                <?php } ?>
                            </div>
                            <!-- logo end -->
                        </div>
                        <div class="col-md-9 col-sm-9">
                            <?php if(!empty($header_button_url)){?>
                            <div class="header-right-link">
                                <!-- search option end -->
								<a class="s-menu" href="<?php echo esc_url($header_button_url);?>"><?php echo esc_html($header_button_text);?></a>
                            </div>
                            <?php } ?>
                            <!-- mainmenu start -->
                            <nav class="navbar navbar-default">
                                <div class="collapse navbar-collapse" id="navbar-example">
                                    <?php
                                        if (has_nav_menu('primary')) {
                                            wp_nav_menu(array(
                                                'theme_location' => 'primary',
                                                'depth' => 3, // 1 = no dropdowns, 2 = with dropdowns.
                                                'container' => 'div',
                                                'container_class' => 'main-menu',
                                                'container_id' => '',
                                                'menu_class' => 'nav navbar-nav navbar-right',
                                            ));
                                        }else{
                                            wp_nav_menu(array(
                                                'depth' => 3, // 1 = no dropdowns, 2 = with dropdowns.
                                                'container' => 'div',
                                                'container_class' => 'main-menu',
                                                'container_id' => '',
                                                'menu_class' => 'nav navbar-nav navbar-right',
                                            ));       
                                        }
                                    ?>
                                </div>
                            </nav>
                            <!-- mainmenu end -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- header-area end -->
            <!-- mobile-menu-area start -->
            <div class="mobile-menu-area hidden-lg hidden-md hidden-sm">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="mobile-menu">
                                <div class="logo">
                                    <?php if (function_exists('get_custom_logo') && has_custom_logo()) {
                                        the_custom_logo();
                                    }else{ ?>
                                    <a href="<?php echo esc_url(home_url('/')); ?>">
                                        <img src="<?php echo esc_url(LITING_IMG_URL . 'logo.png') ?>" alt="logo">
                                    </a>
                                    <?php } ?>
                                </div>
                                <nav id="dropdown">
                                    <?php
                                        if (has_nav_menu('primary')) {
                                            wp_nav_menu(array(
                                                'theme_location' => 'primary',
                                                'depth' => 3, // 1 = no dropdowns, 2 = with dropdowns.
                                            ));
                                        }else{
                                            wp_nav_menu(array(
                                                'depth' => 3, // 1 = no dropdowns, 2 = with dropdowns.
                                            ));       
                                        }
                                    ?>
                                </nav>
                            </div>					
                        </div>
                    </div>
                </div>
            </div>
            <!-- mobile-menu-area end -->		
        </header>
        <!-- header end -->