<?php 
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package liting
 */
get_header();
$liting_header_image_link =  get_the_post_thumbnail_url(get_the_ID(),'full'); 
$title_breadcumb =  get_post_meta(get_the_id(), 'liting_title_breadcumb',true); 


if(!is_front_page()){
    if($title_breadcumb != "off"){
?>
    <div class="page-area image_background" data-image-src="<?php echo esc_url($liting_header_image_link);?>">
        <div class="breadcumb-overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="breadcrumb text-center">
                        <div class="section-headline white-headline text-center">
                            <h3><?php the_title();?></h3>
                        </div>
                        <?php
                        if (function_exists('bcn_display')) {
                        ?>
                        <ul>
                          <?php  bcn_display();?>
                        </ul>
                       <?php }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
    } 
}
?>
    <!--Blog Section-->
        <div class="page-content">
           
                <?php
                    while ( have_posts() ) : the_post();

                    ?>
                    <div  id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                        <?php
                        the_content();

                        wp_link_pages( array(
                            'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'liting' ),
                            'after'  => '</div>',
                        ) );
                        ?>

                    </div>
                <?php

                        // If comments are open or we have at least one comment, load up the comment template.
                        if ( comments_open() || get_comments_number() ) :
                            comments_template();
                        endif;

                    endwhile; // End of the loop.
                    ?>               
               
           
        </div>
       
    <!-- .section -->

<?php get_footer()?>