<?php 
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package liting
 */
get_header();
global $liting_options;
$blog_header_url = isset($liting_options['liting_blog_header_image']['url']) ? $liting_options['liting_blog_header_image']['url'] : '';
$blog_title = isset($liting_options['liting_blog_title']) ? $liting_options['liting_blog_title'] : '';
global $post;
$display_name = get_the_author_meta('display_name', $post->post_author);
$user_avatar = get_avatar($post->post_author, 175);
$blog_banner = isset($liting_options['liting_blog_banner']) ? $liting_options['liting_blog_banner'] : '0';
if($blog_banner == "1"){
?>
<div class="page-area image_background" data-image-src="<?php echo esc_url($blog_header_url);?>">
    <div class="breadcumb-overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="breadcrumb text-center">
                    <div class="section-headline white-headline text-center">
                        <?php if(!empty($blog_title)){?>
                        <h3><?php echo wp_kses_post($blog_title);?></h3>
                        <?php }else{ ?>
                            <h3><?php the_title();?></h3>
                        <?php } ?>
                    </div>
                    <?php
                        if (function_exists('bcn_display')) {
                        ?>
                        <ul>
                          <?php  bcn_display();?>
                        </ul>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php } ?>
<!-- =========single-blog area=========== -->
<div class="blog-area area-padding">
    <div class="container">
        <div class="row">
            <div class="blog-details">
               <?php if (is_active_sidebar('sidebar')) { ?>
                <div class="col-md-8 col-sm-8 col-xs-12">
               <?php }else{ ?>
                    <div class="col-sm-12 margin-auto">
               <?php } ?>
                        
                        <?php
                        while (have_posts()) :
                            the_post();    
                            ?> 
                            <article class="blog-post-wrapper">
                                <div class="blog-banner">
                                    <?php 
                                        if (has_post_thumbnail()) { 
                                            echo the_post_thumbnail('full');
                                        }
                                    ?>
                                    <div class="blog-content">      
                                    <div class="blog-meta">
                                        <span class="date-type">
                                            <i class="fa fa-user"></i>
                                            <?php echo ucfirst(get_the_author());?>
                                        </span>
                                        <span class="date-type">
                                        <?php the_time( get_option( 'date_format' ) ); ?>
                                        </span>
                                        <span class="comments-type">
                                            <i class="fa fa-comment-o"></i>
                                            <?php if (get_comments_number(get_the_ID()) < 10 && get_comments_number(get_the_ID()) != 0 ) { 
                                                echo  '0'.get_comments_number(get_the_ID());
                                            }else{
                                                echo  get_comments_number(get_the_ID());
                                            }
                                            ?>
                                        </span>
                                    </div>                                                                                
                                    <h4><?php the_title();?></h4>
                                    <?php
                                    the_content();
                                    ?>
                                    <div class="tagcloud">
                                    <?php echo get_the_tag_list(); ?>
                                    </div>
                                    <?php
                                    wp_link_pages(array(
                                        'before' => '<div class="page-links">' . esc_html__('Pages:', 'liting'),
                                        'after' => '</div>',
                                    ));
                                    ?>
                                   
                                    </div>
                                </div>
                            </article>
                            <div class="clear"></div>
                            <div class="single-post-comments">
                            <?php
                                // If comments are open or we have at least one comment, load up the comment template.
                                if (comments_open() || get_comments_number()) :
                                    comments_template();
                                endif;
                            endwhile; // End of the loop
                            ?>
                            </div>
                            
                    </div>
                  
                    <!-- Start Right Sidebar blog -->
                    <?php if (is_active_sidebar('sidebar')) { ?>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="left-head-blog right-side">
                            <?php
                                get_sidebar('sidebar');
                            ?>
                        </div>
                    </div>
                    <?php } ?>
                    <!-- End Right Sidebar -->
            </div>
        </div>
    </div>
</div>
<?php get_footer()?>