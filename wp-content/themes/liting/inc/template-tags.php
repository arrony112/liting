<?php
/**
 * Custom template tags for this theme
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package Liting
 */

if (!function_exists('liting_post_meta')) :
    function liting_post_meta(){
        ?>
        <div class="blog-meta">
        <span class="date-type">
            <i class="fa fa-user"></i>
            <?php echo ucfirst(get_the_author());?>
        </span>
        <span class="date-type">
        <?php the_time( get_option( 'date_format' ) ); ?>
        </span>
        <span class="comments-type">
            <i class="fa fa-comment-o"></i>
            <?php if (get_comments_number(get_the_ID()) < 10 && get_comments_number(get_the_ID()) != 0 ) { 
              echo  '0'.get_comments_number(get_the_ID());
            }else{
                echo  get_comments_number(get_the_ID());
            }
            ?>
        </span>
    </div>
<?php   }
endif;