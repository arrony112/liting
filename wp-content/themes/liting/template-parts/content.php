<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package liting
 */
?>

<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 blog-item cat2 cat3 cat4">
    <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <div class="single-blog margin-bb">
           <?php if (has_post_thumbnail()) { ?>
            <!-- blog-image-->
            <div class="blog-image">
                <?php 
                    echo the_post_thumbnail();
                ?>
                <div class="blog-tag">
                    <?php echo liting_categories() ?>
                </div>
            </div>
            <?php  } ?>
            <!-- blog-content-->
            <div class="blog-content main-blog-content">
                <div class="blog-content-heading">
                    <h4><a href="<?php esc_url(the_permalink())?>"><?php the_title();?></a></h4>
                </div>
                <div class="blog-date">
                    <span><?php the_time( get_option( 'date_format' ) ); ?></span>
                </div>
                  <?php
                    if (get_option('rss_use_excerpt')) {
                        the_excerpt();
                    } else {
                        the_excerpt();
                    }
                    wp_link_pages(array(
                        'before' => '<div class="page-links">' . esc_html__('Pages:', 'liting'),
                        'after' => '</div>'                               
                    ));
                   ?>
                <a class="blog-readmore-btn" href="<?php esc_url(the_permalink())?>"><?php esc_html_e('Read More','liting')?></a>
            </div>
        </div>
    </div>
</div>