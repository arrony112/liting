<?php 
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package liting
 */
get_header();

global $liting_options;
$blog_header_url = isset($liting_options['liting_blog_header_image']['url']) ? $liting_options['liting_blog_header_image']['url'] : '';
$blog_title = isset($liting_options['liting_blog_title']) ? $liting_options['liting_blog_title'] : '';

$liting_blog_style = get_query_var('blog_type');

if (!$liting_blog_style) {
    $liting_blog_style = $liting_options['liting_blog_style'];
}
?>
    <div class="page-area image_background" data-image-src="<?php echo esc_url($blog_header_url);?>">
        <div class="breadcumb-overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="breadcrumb text-center">
                        <div class="section-headline white-headline text-center">
                            <h3><?php esc_html_e('Error Page','liting');?></h3>
                        </div>
                        <?php
                        if (function_exists('bcn_display')) {
                        ?>
                        <ul>
                          <?php  bcn_display();?>
                        </ul>
                       <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="blog-area fix area-padding error-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-12 col-sm-12 offset-lg-3 error-column">
                    <div class="content-box">
                        <h1><?php echo esc_html__( '404' , 'liting'); ?></h1>
                        <h2><?php echo esc_html__( 'Page Not Found', 'liting' ); ?></h2>
                        <p><?php echo esc_html__( 'The page you were looking for could not be found. ','liting');?>
                            <a href="<?php echo esc_url(home_url('/')); ?>"> <?php echo esc_html__('Go to Home', 'liting'); ?></a>
                        </p>
                        <?php get_search_form(); ?>
                    </div>
                </div>
            </div>     
        </div>
    </div>    

<?php get_footer()?>