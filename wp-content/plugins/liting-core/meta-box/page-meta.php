<?php

add_filter('rwmb_meta_boxes', 'smartco_register_framework_post_meta_box');

/**
 * Register meta boxes
 *
 * Remember to change "your_prefix" to actual prefix in your project
 *
 * @return void
 */
function smartco_register_framework_post_meta_box($meta_boxes) {

    global $wp_registered_sidebars;


    $sidebars = array(
        '0' => esc_html__('Default widget', 'liting-core')
    );

    foreach ($wp_registered_sidebars as $key => $value) {
        $sidebars[$key] = $value['name'];
    }


    /**
     * prefix of meta keys (optional)
     * Use underscore (_) at the beginning to make keys hidden
     * Alt.: You also can make prefix empty to disable it
     */
    // Better has an underscore as last sign
    $prefix = 'liting';


    $posts_page = get_option('page_for_posts');

    if (!isset($_GET['post']) || intval($_GET['post']) != $posts_page) {

        $meta_boxes[] = array(
            'id' => $prefix . '_page_meta_box',
            'title' => esc_html__('Page Design Settings', 'liting-core'),
            'pages' => array(
                'page',
            ),
            'context' => 'normal',
            'priority' => 'core',
            'fields' => array(
                array(
                    'id' => "{$prefix}_title_breadcumb",
                    'name' => esc_html__('Title & Breadcumb', 'liting-core'),
                    'desc' => '',
                    'type' => 'select',
                    'std' => "on",
                    'options' => array(
                        'on' => 'On',
                        'off' => 'Off',
                    ),
                    'allowClear' => true,
                    'placeholder' => esc_html__('Select', 'liting-core'),
                ),
                array(
                    'id' => "{$prefix}_color_select",
                    'name' => esc_html__('Color Select', 'liting-core'),
                    'desc' => '',
                    'type' => 'select',
                    'std' => "blue",
                    'options' => array(
                        'blue' => 'Blue',
                        'red' => 'Red',
                        'green' => 'Green',
                    ),
                    'allowClear' => true,
                    'placeholder' => esc_html__('Select', 'liting-core'),
                ),
            )
        );
    }


    return $meta_boxes;
}
