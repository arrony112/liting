<?php
/*
  Plugin Name: Liting Core
  Plugin URI: http://themepuller.com/
  Description: Liting Core theme functions and library files.
  Version: 1.0
  Author: themepuller
  Author URI: http://themepuller.com/
  License: GPLv2 or later
  Text Domain: liting-core
  Domain Path: /languages/
 */

define('PLUGIN_DIR', dirname(__FILE__) . '/');

if (!defined('LITING_CORE_PLUGIN_URI')) {
    define('LITING_CORE_PLUGIN_URI', plugin_dir_url(__FILE__));
}

require_once(PLUGIN_DIR . "breadcrumb-navxt/breadcrumb-navxt.php" );

if (!class_exists('LitingCore')) {

    class LitingCore {

        public static $plugindir, $pluginurl;

        function __construct() {

            LitingCore::$plugindir = dirname(__FILE__);

            LitingCore::$pluginurl = plugins_url('', __FILE__);
        }


    }

    $LitingCore = new LitingCore();


    require_once( LitingCore::$plugindir . "/widgets/liting-latest-post-widget.php" );
    require_once( LitingCore::$plugindir . "/widgets/liting-social-link-widget.php" );
    require_once( LitingCore::$plugindir . "/meta-box/page-meta.php" );


    require_once( LitingCore::$plugindir . "/elementor-addons/liting-elementor.php" );

}

function liting_core_load_textdomain() {
    load_plugin_textdomain('liting-core', false, dirname(__FILE__) . "/languages");
}


add_action('plugins_loaded', 'liting_core_load_textdomain');
