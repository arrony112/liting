(function ($) {
    "use strict";
    var LitingTestimonialSlider = function ($scope, $) {
        
        var review = $('.testimonial-carousel');
        review.owlCarousel({
            loop:true,
            nav:false,
            margin:30,
            center:true,
            dots:true,
            autoplay:false,
            responsive:{
                0:{
                    items:1
                },
                768:{
                    items:2
                },
                1000:{
                    items:3
                }
            }
        });
        
    }





    $(window).on('elementor/frontend/init', function () {
        elementorFrontend.hooks.addAction('frontend/element_ready/LitingTestimonialSlider.default', LitingTestimonialSlider);
    });
})(jQuery);