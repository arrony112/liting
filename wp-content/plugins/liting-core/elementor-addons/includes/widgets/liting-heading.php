<?php
  use Elementor\Utils;

  class LitingHeading extends \Elementor\Widget_Base {

    public function get_name() {
    return 'LitingHeading';
  }

  public function get_title() {
    return esc_html__( 'Liting Heading', 'liting-core' );
  }

  public function get_icon() {
    return '';
  }

   public function get_categories() {
    return [ 'liting' ];
  }
  
    protected function _register_controls() {

      $this->start_controls_section(
        'content',
        [
          'label' => __( 'Content', 'liting-core' ),
        ]
     );
 
         $this->add_control(
           'title',
           [
             'label' => __( 'Title', 'liting-core' ),
             'type' => \Elementor\Controls_Manager::TEXT,
           ]
         );
         $this->add_control(
          'icon',
          [
            'label' => __( 'Icon', 'liting-core' ),
            'type' => \Elementor\Controls_Manager::ICON,
          ]
        );
         $this->add_control(
          'add_class',
          [
            'label' => __( 'Add Class', 'liting-core' ),
            'type' => \Elementor\Controls_Manager::TEXT,
            
          ]
        );

      $this->end_controls_section();    

    }    
    
    protected function render() {
      $settings =  $this->get_settings_for_display();

     
	      
	      $title = $settings["title"];  
        $icon = $settings["icon"];  
        $add_class = $settings["add_class"];  
    

    ?>

    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12 wow fadeInUp" data-wow-delay="0.4s">
        <div class="section-headline <?php echo $add_class;?>">
          <span class="title-icon"><i class="<?php echo $icon;?>"></i></span>
          <h3><?php echo $title;?></h3>
        </div>
      </div>
    </div>

    <?php
    }
    protected function _content_template() {
      
    }
  }

  \Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \LitingHeading() );
