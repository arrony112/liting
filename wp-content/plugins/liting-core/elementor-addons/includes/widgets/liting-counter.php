<?php
  use Elementor\Utils;

  class LitingCounter extends \Elementor\Widget_Base {

    public function get_name() {
    return 'LitingCounter';
  }

  public function get_title() {
    return esc_html__( 'Liting Counter', 'liting-core' );
  }

  public function get_icon() {
    return '';
  }

   public function get_categories() {
    return [ 'liting' ];
  }
  
    protected function _register_controls() {

      $this->start_controls_section(
        'content',
        [
          'label' => __( 'Content', 'liting-core' ),
        ]
     );
         $this->add_control(
          'add_class',
          [
            'label' => __( 'Add Class', 'liting-core' ),
            'type' => \Elementor\Controls_Manager::TEXT,
            'default' => __( '', 'liting-core' ),
            
          ]
        );

      $this->end_controls_section();    

      $this->start_controls_section(
         'counter_box',
         [
           'label' => __( 'Counter Box', 'liting-core' ),
         ]
      );
      $repeater = new \Elementor\Repeater();
      $repeater->add_control(
        'title',
        [
          'label' => __( 'Title', 'liting-core' ),
          'type' => \Elementor\Controls_Manager::TEXT,
            'default' => __( 'Projects complete', 'liting-core' ),
            
        ]
      );
      $repeater->add_control(
        'counter_number',
        [
                'label' => __( 'Counter Number', 'liting-core' ),
                'type' => \Elementor\Controls_Manager::TEXT,
                'rows' => 0,
                'default' => __( '980', 'liting-core' ),
                
            ]
        );
        $repeater->add_control(
          'icon',
          [
            'label' => __( 'Icon', 'liting-core' ),
            'type' => \Elementor\Controls_Manager::ICON,
          ]
        );

      $this->add_control(
      'items1',
      [
        'label' => __( 'Repeater List', 'liting-core' ),
        'type' => \Elementor\Controls_Manager::REPEATER,
        'fields' => $repeater->get_controls(),
        'default' => [
          [
            'list_title' => __( 'Title #1', 'liting-core' ),
            'list_content' => __( 'Item content. Click the edit button to change this text.', 'liting-core' ),
          ],
          [
            'list_title' => __( 'Title #2', 'liting-core' ),
            'list_content' => __( 'Item content. Click the edit button to change this text.', 'liting-core' ),
          ],
        ],
      ]
    );
      $this->end_controls_section();

    }    
    
    protected function render() {
      $settings =  $this->get_settings_for_display();
	    $add_class = $settings["add_class"];
    ?>
    <!-- pricing-section -->
    <div class="row">
        <div class="fun-content <?php echo esc_attr($add_class);?>">       
                <?php 
                  $i = 1;
                  foreach($settings["items1"] as $item){
                    $title = $item["title"]; 
                    $counter_number = $item["counter_number"]; 
                    $icon = $item["icon"]; 
                    $i++;
                ?>  
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class="fun_text wow fadeInLeft" data-wow-delay="0.<?php echo $i;?>s">
                              <span class="counter-icon"><i class="<?php echo $icon;?>"></i></span>
                              <span class="counter"><?php echo esc_html($counter_number);?></span>
                              <h4><?php echo esc_html($title);?></h4>
                          </div>
                      </div>
                <?php } ?>
          </div>
      </div>
    <?php
    }
    protected function _content_template() {
      
    }
  }

  \Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \LitingCounter() );
