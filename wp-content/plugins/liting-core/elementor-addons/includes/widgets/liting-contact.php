<?php
  use Elementor\Utils;

  class SmartcoContact extends \Elementor\Widget_Base {

    public function get_name() {
    return 'contact';
  }

  public function get_title() {
    return esc_html__( 'Contact', 'smartco-core' );
  }

  public function get_icon() {
    return '';
  }

   public function get_categories() {
    return [ 'smartco' ];
  }
  
    protected function _register_controls() {

      $this->start_controls_section(
         'contact',
         [
           'label' => __( 'Contact', 'smartco-core' ),
         ]
      );
            $this->add_control(
              'title',
              [
                'label' => __( 'Title', 'liting-core' ),
                'type' => \Elementor\Controls_Manager::TEXT,
                'default' => __( '', 'smartco-core' ),
              ]
            );
            $this->add_control(
              'extra_class',
              [
                'label' => __( 'Extra Class', 'liting-core' ),
                'type' => \Elementor\Controls_Manager::TEXT,
                'default' => __( '', 'smartco-core' ),
              ]
            );

      $this->end_controls_section();
      $repeater = new \Elementor\Repeater();
          $repeater->add_control(
            'content',
            [
              'label' => __( 'Content', 'liting-core' ),
              'type' => \Elementor\Controls_Manager::TEXTAREA,
            ]
          );
          $repeater->add_control(
            'icon',
            [
              'label' => __( 'Icon', 'liting-core' ),
              'type' => \Elementor\Controls_Manager::ICON,
            ]
          );
          $this->end_controls_section();
          $this->start_controls_section(
            'contact_list',
            [
              'label' => __( 'Contact List', 'liting-core' ),
            ]
         );
       
           $this->add_control(
             'items1',
             [
               'label' => __( 'Repeater List', 'liting-core' ),
               'type' => \Elementor\Controls_Manager::REPEATER,
               'fields' => $repeater->get_controls(),
               'default' => [
                 [
                   'list_title' => __( 'Title #1', 'liting-core' ),
                   'list_content' => __( 'Item content. Click the edit button to change this text.', 'liting-core' ),
                 ],
                 [
                   'list_title' => __( 'Title #2', 'liting-core' ),
                   'list_content' => __( 'Item content. Click the edit button to change this text.', 'liting-core' ),
                 ],
               ],
             ]
           );
         $this->end_controls_section();
  
    }    
    protected function render() {
      $settings =  $this->get_settings_for_display(); 
      $title = $settings["title"]; 
      $extra_class = $settings["extra_class"];
      $items1 =    $settings["items1"];  
      
?>
<div class="office-city">
    <h4><?php echo $title;?></h4>
    <?php 
    foreach($items1 as $key=> $item){ 
      $icon = $item["icon"]; 
      $content = $item["content"]; 
      ?>
    <div class="contact-icon">
        <div class="single-icon">
            <i class="<?php echo esc_attr($icon);?>"></i>
            <p>
                <?php echo wp_kses_post($content);?>
            </p>
        </div>
    </div>
    <?php } ?>
 
</div>


 <?php 
    }
  
    protected function _content_template() {
      
    }
  }

\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \SmartcoContact() );