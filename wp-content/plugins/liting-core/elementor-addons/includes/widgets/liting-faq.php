<?php
  use Elementor\Utils;

  class LitingFaq extends \Elementor\Widget_Base {

    public function get_name() {
    return 'LitingFaq';
  }

  public function get_title() {
    return esc_html__( 'Liting Faq', 'liting-core' );
  }

  public function get_icon() {
    return '';
  }

   public function get_categories() {
    return [ 'liting' ];
  }
  
    protected function _register_controls() {

      $this->start_controls_section(
        'content',
        [
          'label' => __( 'Content', 'liting-core' ),
        ]
     );
         $this->add_control(
          'add_id',
          [
            'label' => __( 'Add Id', 'liting-core' ),
            'type' => \Elementor\Controls_Manager::TEXT,
            'default' => __( 'accordion', 'liting-core' ),
            
          ]
        );
        $this->add_control(
          'add_class',
          [
            'label' => __( 'Add Class', 'liting-core' ),
            'type' => \Elementor\Controls_Manager::TEXT,
            'default' => __( 'left-faq', 'liting-core' ),
            
          ]
        );

      $this->end_controls_section();    

      $this->start_controls_section(
         'faq_list',
         [
           'label' => __( 'Faq list', 'liting-core' ),
         ]
      );
      $repeater = new \Elementor\Repeater();
      $repeater->add_control(
        'title',
        [
          'label' => __( 'Title', 'liting-core' ),
          'type' => \Elementor\Controls_Manager::TEXT,
          'default' => __( 'How to successful our mission', 'liting-core' ),
            
        ]
      );
      $repeater->add_control(
        'desc',
        [
                'label' => __( 'Desc', 'liting-core' ),
                'type' => \Elementor\Controls_Manager::TEXTAREA,
                'rows' => 0,
                'default' => __( 'When replacing a multi-lined selection of text, the generated dummy text maintains the amount of lines. When replacing a selection of text within a single line, the amount of words is roughly being maintained.', 'liting-core' ),
                
            ]
        );

      $this->add_control(
      'items1',
      [
        'label' => __( 'Repeater List', 'liting-core' ),
        'type' => \Elementor\Controls_Manager::REPEATER,
        'fields' => $repeater->get_controls(),
        'default' => [
          [
            'list_title' => __( 'Title #1', 'liting-core' ),
            'list_content' => __( 'Item content. Click the edit button to change this text.', 'liting-core' ),
          ],
          [
            'list_title' => __( 'Title #2', 'liting-core' ),
            'list_content' => __( 'Item content. Click the edit button to change this text.', 'liting-core' ),
          ],
        ],
      ]
    );
      $this->end_controls_section();

    }    
    
    protected function render() {
      $settings =  $this->get_settings_for_display();

	    $add_id = $settings["add_id"];  
	    $add_class = $settings["add_class"];  
    

    ?>

          <div class="company-faq <?php echo esc_attr($add_class);?>">
              <div class="faq-full">
								<div class="faq-details">
									<div class="panel-group" id="<?php echo $add_id;?>">
                  <?php 
                  $i = 0;
                  foreach($settings["items1"] as $item){
                    $i++;
                    $title = $item["title"]; 
                    $desc = $item["desc"]; 
                  ?>  
										<!-- Panel Default -->
										<div class="panel panel-default">
											<div class="panel-heading">
												<h4 class="check-title">
													<a data-toggle="collapse" class="active" data-parent="#<?php echo $add_id;?>" href="#<?php echo $add_id.$i;?>">
														<span class="acc-icons"></span><?php echo $title;?>
													</a>
												</h4>
											</div>
											<div id="<?php echo $add_id.$i;?>" class="panel-collapse collapse <?php if($i == 1){ echo 'in';}?>">
												<div class="panel-body">
													<p>
                          <?php echo $desc;?> 
													</p>		
												</div>
											</div>
										</div>
										<!-- End Panel Default -->	
                    <?php } ?>
									</div>
								</div>
								<!-- End FAQ -->
							</div>
          </div>


    <?php
    }
    protected function _content_template() {
      
    }
  }

  \Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \LitingFaq() );
