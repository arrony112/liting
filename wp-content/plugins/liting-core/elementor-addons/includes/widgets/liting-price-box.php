<?php
  use Elementor\Utils;

  class LitingPriceBox extends \Elementor\Widget_Base {

    public function get_name() {
    return 'LitingPriceBox';
  }

  public function get_title() {
    return esc_html__( 'Liting Price Box', 'liting-core' );
  }

  public function get_icon() {
    return '';
  }

   public function get_categories() {
    return [ 'liting' ];
  }
  
    protected function _register_controls() {

      $this->start_controls_section(
        'content',
        [
          'label' => __( 'Content', 'liting-core' ),
        ]
     );
         $this->add_control(
          'add_class',
          [
            'label' => __( 'Add Class', 'liting-core' ),
            'type' => \Elementor\Controls_Manager::TEXT,
            'default' => __( '', 'liting-core' ),
            
          ]
        );

      $this->end_controls_section();    

      $this->start_controls_section(
         'price_box',
         [
           'label' => __( 'Price Box', 'liting-core' ),
         ]
      );
      $repeater = new \Elementor\Repeater();
      $repeater->add_control(
        'price_title',
        [
          'label' => __( 'Price Title', 'liting-core' ),
          'type' => \Elementor\Controls_Manager::TEXT,
            'default' => __( 'Basic', 'liting-core' ),
            
        ]
      );
      $repeater->add_control(
        'price',
        [
                'label' => __( 'Price', 'liting-core' ),
                'type' => \Elementor\Controls_Manager::TEXT,
                'rows' => 0,
                'default' => __( '15', 'plugin-domain' ),
                
            ]
        );
        $repeater->add_control(
          'duration',
          [
                  'label' => __( 'Duration', 'liting-core' ),
                  'type' => \Elementor\Controls_Manager::TEXT,
                  'rows' => 0,
                  'default' => __( 'Per Month', 'plugin-domain' ),
                  
              ]
          );
        $repeater->add_control(
          'price_list',
          [
            'label' => __( 'Price List', 'liting-core' ),
            'type' => \Elementor\Controls_Manager::WYSIWYG,
            'default' => __( 'Default description', 'liting-core' ),
            'placeholder' => __( 'Type your description here', 'liting-core' ),
             
          ]
        );
       $repeater->add_control(
            'link',
            [
                'label' => __( 'Link', 'liting-core' ),
                'type' => \Elementor\Controls_Manager::URL,
                'placeholder' => __( 'https://your-link.com', 'liting-domain' ),
                'show_external' => true,
                'default' => [
                  'url' => '',
                  'is_external' => true,
                  'nofollow' => true,
                ],
                
              ]
            );
      $this->add_control(
      'items1',
      [
        'label' => __( 'Repeater List', 'liting-core' ),
        'type' => \Elementor\Controls_Manager::REPEATER,
        'fields' => $repeater->get_controls(),
        'default' => [
          [
            'list_title' => __( 'Title #1', 'liting-core' ),
            'list_content' => __( 'Item content. Click the edit button to change this text.', 'liting-core' ),
          ],
          [
            'list_title' => __( 'Title #2', 'liting-core' ),
            'list_content' => __( 'Item content. Click the edit button to change this text.', 'liting-core' ),
          ],
        ],
      ]
    );
      $this->end_controls_section();

    }    
    
    protected function render() {
      $settings =  $this->get_settings_for_display();
	      $add_class = $settings["add_class"];  

    ?>


    <!-- pricing-section -->
    <div class="row">
        <div class="pricing-content <?php echo  esc_attr($add_class);?>">
                 
                  <?php 
                  $i = 1;
                  foreach($settings["items1"] as $item){
                    $i++;
                    $i = $i+1;
                    $price_title = $item["price_title"]; 
                    $price = $item["price"]; 
                    $duration = $item["duration"]; 
                    $price_list = $item["price_list"]; 
                    $link = $item["link"]['url']; 
                        ?>  
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="table-list wow fadeInUp" data-wow-delay="0.<?php echo $i;?>s">
                                <div class="top-price-inner">
                                    <h4><?php echo esc_html($price_title);?></h4>
                                    <div class="rates">
                                        <span class="prices"><span class="dolar">$</span><?php echo esc_html($price);?></span><span class="users"><?php echo esc_html($duration);?></span>
                                    </div>
                                </div>
                                <ol>
                                <?php echo wp_kses_post( $price_list );?>
                                </ol>
                                <div class="price-btn">
                                    <a href="<?php echo esc_url($link);?>"><?php esc_html_e('Buy now','liting-core');?></a>
                                </div>
                            </div>
                        </div>

                    <?php } ?>
          </div>
      </div>

    <?php
    }
    protected function _content_template() {
      
    }
  }

  \Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \LitingPriceBox() );
