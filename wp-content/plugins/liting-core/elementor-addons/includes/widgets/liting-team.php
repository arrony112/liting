<?php
  use Elementor\Utils;

  class LitingTeam extends \Elementor\Widget_Base {

    public function get_name() {
    return 'LitingTeam';
  }

  public function get_title() {
    return esc_html__( 'Liting Team', 'liting-core' );
  }

  public function get_icon() {
    return '';
  }

   public function get_categories() {
    return [ 'liting' ];
  }
  
    protected function _register_controls() {

      $this->start_controls_section(
         'content',
         [
           'label' => __( 'Content', 'liting-core' ),
         ]
      );
            $this->add_control(
              'extra_class',
              [
                'label' => __( 'Extra Class', 'liting-core' ),
                'type' => \Elementor\Controls_Manager::TEXT,
                'default' => __( '', 'liting-core' ),
              ]
            );
          $repeater = new \Elementor\Repeater();
          $repeater->add_control(
            'name',
            [
              'label' => __( 'Name', 'liting-core' ),
              'type' => \Elementor\Controls_Manager::TEXT,
              'default' => __( 'Jackobs', 'liting-core' ),
            ]
          );
          $repeater->add_control(
            'designation',
            [
              'label' => __( 'Designation', 'liting-core' ),
              'type' => \Elementor\Controls_Manager::TEXT,
              'default' => __( 'Co-Founder', 'liting-core' ),
            ]
          );
          $repeater->add_control(
            'image',
            [
              'label' => __( 'Image', 'liting-core' ),
              'type' => \Elementor\Controls_Manager::MEDIA,
              'default' => [
                        'url' => Utils::get_placeholder_image_src(),
                    ],
              
            ]
          );
          $repeater->add_control(
            'facebook_url',
            [
                'label' => __( 'Facebook Url', 'smartco' ),
                'type' => \Elementor\Controls_Manager::URL,
                'placeholder' => __( 'https://your-link.com', 'liting-core' ),
                'show_external' => true,
                'default' => [
                  'url' => '',
                  'is_external' => true,
                  'nofollow' => true,
                ],
                
              ]
            );
            $repeater->add_control(
              'twitter_url',
              [
                  'label' => __( 'Twitter Url', 'smartco' ),
                  'type' => \Elementor\Controls_Manager::URL,
                  'placeholder' => __( 'https://your-link.com', 'liting-core' ),
                  'show_external' => true,
                  'default' => [
                    'url' => '',
                    'is_external' => true,
                    'nofollow' => true,
                  ],
                  
                ]
              );
              $repeater->add_control(
                'instagram_url',
                [
                    'label' => __( 'Instagram Url', 'smartco' ),
                    'type' => \Elementor\Controls_Manager::URL,
                    'placeholder' => __( 'https://your-link.com', 'liting-core' ),
                    'show_external' => true,
                    'default' => [
                      'url' => '',
                      'is_external' => true,
                      'nofollow' => true,
                    ],
                    
                  ]
                );
  
      $this->end_controls_section();

      $this->start_controls_section(
        'team_member_list',
        [
          'label' => __( 'Team Member List', 'liting-core' ),
        ]
      );
      $this->add_control(
        'items1',
        [
          'label' => __( 'Repeater List', 'liting-core' ),
          'type' => \Elementor\Controls_Manager::REPEATER,
          'fields' => $repeater->get_controls(),
          'default' => [
            [
              'list_title' => __( 'Title #1', 'liting-core' ),
              'list_content' => __( 'Item content. Click the edit button to change this text.', 'liting-core' ),
            ],
            [
              'list_title' => __( 'Title #2', 'liting-core' ),
              'list_content' => __( 'Item content. Click the edit button to change this text.', 'liting-core' ),
            ],
          ],
        ]
      );
  
      $this->end_controls_section();
  
    }    
    protected function render() {
      $settings =  $this->get_settings_for_display(); 
      $extra_class = $settings["extra_class"]; 
      
?>

<div class="row <?php echo esc_attr($extra_class);?>">
    <div class="team-member">
        <?php foreach($settings["items1"] as $item){ 
              $name = $item["name"]; 
              $designation = $item["designation"]; 
              $image = $item[ 'image']['url']; 
              $facebook_url = $item[ 'facebook_url']['url']; 
              $twitter_url = $item[ 'twitter_url']['url']; 
              $instagram_url = $item[ 'instagram_url']['url']; 
              ?>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="single-member">
                    <div class="team-img">
                        <img src="<?php echo esc_url($image);?>" alt="">
                    </div>
                    <div class="team-content text-center">
                        <h4><?php echo esc_html($name);?></h4>
                        <p><?php echo esc_html($designation);?></p>
                        <ul class="social-icon">
                            <li><a class="facebook" href="<?php echo esc_url($facebook_url);?>"><i class="fa fa-facebook"></i></a></li>
                            <li><a class="twitter" href="<?php echo esc_url($twitter_url);?>"><i class="fa fa-twitter"></i></a></li>
                            <li><a class="instagram" href="<?php echo esc_url($instagram_url);?>"><i class="fa fa-instagram"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>


 <?php 
    }
  
    protected function _content_template() {
      
    }
  }

\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \LitingTeam() );