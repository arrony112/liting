<?php
  use Elementor\Utils;

  class LitingService extends \Elementor\Widget_Base {

    public function get_name() {
    return 'LitingService';
  }

  public function get_title() {
    return esc_html__( 'Liting Service', 'liting-core' );
  }

  public function get_icon() {
    return '';
  }

   public function get_categories() {
    return [ 'liting' ];
  }
  
    protected function _register_controls() {

      $this->start_controls_section(
         'content',
         [
           'label' => __( 'Content', 'liting-core' ),
         ]
      );

            $this->add_control(
              'title',
              [
                'label' => __( 'Title', 'liting-core' ),
                'type' => \Elementor\Controls_Manager::TEXT,
              ]
            );
            $this->add_control(
            'icon',
            [
              'label' => __( 'Icon', 'liting-core' ),
              'type' => \Elementor\Controls_Manager::ICON,
            ]
          );
          $this->add_control(
            'desc',
            [
              'label' => __( 'Description', 'liting-core' ),
              'type' => \Elementor\Controls_Manager::TEXTAREA,
            ]
          );
          $this->add_control(
            'image',
            [
              'label' => __( 'Image', 'liting-core' ),
              'type' => \Elementor\Controls_Manager::MEDIA,
              'default' => [
                        'url' => Utils::get_placeholder_image_src(),
                    ],
              
            ]
          );
          $this->add_control(
            'img_position', [
            'label' => esc_html__('Image Position', 'liting-core'),
            'type' => \Elementor\Controls_Manager::SELECT,
            'default' => 'right',
            'options' => [
                'right' => esc_html__('Right', 'liting-core'),
                'left' => esc_html__('Left', 'liting-core')
              ]
            ]
          );
            $this->add_control(
              'extra_class',
              [
                'label' => __( 'Extra Class', 'liting-core' ),
                'type' => \Elementor\Controls_Manager::TEXT,
                'default' => __( '', 'liting-core' ),
              ]
            );
          $repeater = new \Elementor\Repeater();
          $repeater->add_control(
            'title',
            [
              'label' => __( 'Title', 'liting-core' ),
              'type' => \Elementor\Controls_Manager::TEXT,
            ]
          );
          $repeater->add_control(
            'link',
            [
                'label' => __( 'Link', 'liting-core' ),
                'type' => \Elementor\Controls_Manager::URL,
                'placeholder' => __( 'https://your-link.com', 'liting-domain' ),
                'show_external' => true,
                'default' => [
                  'url' => '',
                  'is_external' => true,
                  'nofollow' => true,
                ],
                
              ]
            );

      $this->end_controls_section();

      $this->start_controls_section(
        'service_list',
        [
          'label' => __( 'Service List', 'liting-core' ),
        ]
      );
      $this->add_control(
        'items1',
        [
          'label' => __( 'Repeater List', 'liting-core' ),
          'type' => \Elementor\Controls_Manager::REPEATER,
          'fields' => $repeater->get_controls(),
          'default' => [
            [
              'list_title' => __( 'Title #1', 'liting-core' ),
              'list_content' => __( 'Item content. Click the edit button to change this text.', 'liting-core' ),
            ],
            [
              'list_title' => __( 'Title #2', 'liting-core' ),
              'list_content' => __( 'Item content. Click the edit button to change this text.', 'liting-core' ),
            ],
          ],
        ]
      );
  
      $this->end_controls_section();
  
    }    
    protected function render() {
      $settings =  $this->get_settings_for_display(); 
      $extra_class = $settings["extra_class"]; 
      $title = $settings["title"]; 
      $desc = $settings["desc"]; 
      $icon = $settings["icon"]; 
      $image = $settings["image"]["url"]; 
      $img_position = $settings["img_position"]; 

      $animate_class = '';
      if($img_position == 'right'){
        $animate_class = 'fadeInLeft';
      }else{
        $animate_class = 'fadeInRight';
      }

      
?>      
    <div class="row <?php echo esc_attr($extra_class);?>">
        <?php if($img_position == "left"){?>
        <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="feature-image item-bounce wow <?php echo $animate_class;?>" data-wow-delay="0.3s">
                <img src="<?php echo esc_url($image);?>" alt="">
            </div>
        </div>
        <?php } ?>
        <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="feature-inner wow <?php echo $animate_class;?>" data-wow-delay="0.3s">
                <div class="feature-text">
                    <h3> <span class="top-head"><i class="<?php echo esc_attr($icon);?>"></i></span> <?php echo esc_html($title);?></h3>
                    <p><?php echo esc_html($desc);?></p>
                    <ul>
                    <?php 
                      $i = 0;
                    foreach($settings["items1"] as $item){ 
                      $title = $item["title"]; 
                      $link = $item["link"]['url']; 
                      $i++;
                      ?>
                        <li><a href="<?php echo esc_url($link);?>"><span><?php echo $i;?></span><?php echo esc_html($title);?></a></li>
                    <?php } ?>
                    </ul>
                </div>
            </div>
        </div>
        <?php if($img_position == "right"){?>
        <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="feature-image item-bounce wow <?php echo $animate_class;?>" data-wow-delay="0.3s">
                <img src="<?php echo esc_url($image);?>" alt="">
            </div>
        </div>
        <?php } ?>
    </div>
    <!-- end Row -->



 <?php 
    }
  
    protected function _content_template() {
      
    }
  }

\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \LitingService() );