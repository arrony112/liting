<?php
  use Elementor\Utils;

  class LitingServiceVideo extends \Elementor\Widget_Base {

    public function get_name() {
    return 'LitingServiceVideo';
  }

  public function get_title() {
    return esc_html__( 'Liting Service Video', 'liting-core' );
  }

  public function get_icon() {
    return '';
  }

   public function get_categories() {
    return [ 'liting' ];
  }
  
    protected function _register_controls() {

      $this->start_controls_section(
         'content',
         [
           'label' => __( 'Content', 'liting-core' ),
         ]
      );

            $this->add_control(
              'title',
              [
                'label' => __( 'Title', 'liting-core' ),
                'type' => \Elementor\Controls_Manager::TEXT,
              ]
            );
          $this->add_control(
            'image',
            [
              'label' => __( 'Video Image', 'liting-core' ),
              'type' => \Elementor\Controls_Manager::MEDIA,
              'default' => [
                        'url' => Utils::get_placeholder_image_src(),
                    ],
              
            ]
          );
          $this->add_control(
            'image2',
            [
              'label' => __( 'Video Image 2', 'liting-core' ),
              'type' => \Elementor\Controls_Manager::MEDIA,
              'default' => [
                        'url' => Utils::get_placeholder_image_src(),
                    ],
              
            ]
          );
          $this->add_control(
            'video_url',
            [
              'label' => __( 'Video Url', 'liting-core' ),
              'type' => \Elementor\Controls_Manager::TEXT,
            ]
          );
            $this->add_control(
              'extra_class',
              [
                'label' => __( 'Extra Class', 'liting-core' ),
                'type' => \Elementor\Controls_Manager::TEXT,
                'default' => __( '', 'liting-core' ),
              ]
            );
          $repeater = new \Elementor\Repeater();
          $repeater->add_control(
            'title',
            [
              'label' => __( 'Title', 'liting-core' ),
              'type' => \Elementor\Controls_Manager::TEXT,
            ]
          );
          $repeater->add_control(
            'desc',
            [
              'label' => __( 'Description', 'liting-core' ),
              'type' => \Elementor\Controls_Manager::TEXTAREA,
            ]
          );
          $repeater->add_control(
            'link',
            [
                'label' => __( 'Link', 'liting-core' ),
                'type' => \Elementor\Controls_Manager::URL,
                'placeholder' => __( 'https://your-link.com', 'liting-domain' ),
                'show_external' => true,
                'default' => [
                  'url' => '',
                  'is_external' => true,
                  'nofollow' => true,
                ],
                
              ]
            );

      $this->end_controls_section();

      $this->start_controls_section(
        'service_list',
        [
          'label' => __( 'Service List', 'liting-core' ),
        ]
      );
      $this->add_control(
        'items1',
        [
          'label' => __( 'Repeater List', 'liting-core' ),
          'type' => \Elementor\Controls_Manager::REPEATER,
          'fields' => $repeater->get_controls(),
          'default' => [
            [
              'list_title' => __( 'Title #1', 'liting-core' ),
              'list_content' => __( 'Item content. Click the edit button to change this text.', 'liting-core' ),
            ],
            [
              'list_title' => __( 'Title #2', 'liting-core' ),
              'list_content' => __( 'Item content. Click the edit button to change this text.', 'liting-core' ),
            ],
          ],
        ]
      );
  
      $this->end_controls_section();


      $this->start_controls_section(
        'button',
        [
          'label' => __( 'Button', 'liting-core' ),
        ]
     );

     $this->add_control(
      'button1',
      [
          'label' => __( 'Create an account Url', 'liting-core' ),
          'type' => \Elementor\Controls_Manager::URL,
          'placeholder' => __( 'https://your-link.com', 'liting-domain' ),
          'show_external' => true,
          'default' => [
            'url' => '',
            'is_external' => true,
            'nofollow' => true,
          ],
          
        ]
      );

      $this->add_control(
        'button2',
        [
            'label' => __( 'More about us Url', 'liting-core' ),
            'type' => \Elementor\Controls_Manager::URL,
            'placeholder' => __( 'https://your-link.com', 'liting-domain' ),
            'show_external' => true,
            'default' => [
              'url' => '',
              'is_external' => true,
              'nofollow' => true,
            ],
            
          ]
        );

     $this->end_controls_section();

  
    }    
    protected function render() {
      $settings =  $this->get_settings_for_display(); 
      $extra_class = $settings["extra_class"]; 
      $title = $settings["title"];
      $image = $settings["image"]["url"]; 
      $image2 = $settings["image2"]["url"]; 
      $video_url = $settings["video_url"]; 
      $button1 = $settings["button1"]["url"]; 
      $button2 = $settings["button2"]["url"]; 
      
?>      
    <div class="row <?php echo esc_attr($extra_class);?>">
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="about-content">
                <div class="about-images">
                    <img src="<?php echo esc_url($image);?>" alt="">
                        <img src="<?php echo esc_url($image2);?>" alt="" class="over-img">
                    <div class="video-content">
                        <a href="<?php echo esc_url($video_url);?>" class="video-play vid-zone">
                            <i class="fa fa-play"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="support-all">
                <?php 
                  $i = 0;
                  $a = 1;
                foreach($settings["items1"] as $item){ 
                  $title = $item["title"]; 
                  $desc = $item["desc"]; 
                  $link = $item["link"]["url"]; 
                  $i++;
                  $a++;
                  $a = $a+1;
                  ?>
                    <div class="support-services wow fadeInRight" data-wow-delay="0.<?php echo $a;?>s">
                        <a class="support-images" href="#"><?php echo '0'.$i;?></a>
                        <div class="support-content">
                            <h4><a href="<?php echo esc_url($link);?>"><?php echo esc_html($title);?></a></h4>
                            <p><?php echo esc_html($desc);?></p>
                        </div>
                    </div>
                <?php } ?>

                    <div class="about-btn wow fadeInRight" data-wow-delay="0.7s">
                        <a class="ab-btn ab-1-btn" href="<?php echo esc_url($button1);?>"><?php esc_html_e('Create an account','liting-core');?></a>
                        <a class="ab-btn ab-2-btn" href="<?php echo esc_url($button2);?>"><?php esc_html_e('More about us','liting-core');?></a>
                    </div>

            </div>
        </div>
    </div>
    <!-- end Row -->



 <?php 
    }
  
    protected function _content_template() {
      
    }
  }

\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \LitingServiceVideo() );