<?php
if (!defined('ABSPATH')) {
    exit;
}

use Elementor\Controls_Manager;
use Elementor\Plugin;
use Elementor\Utils;
use Elementor\Widget_Base;

class LitingBlog extends Widget_Base {

    public function get_name() {
        return 'LitingBlog';
    }

    public function get_title() {
        return esc_html__('Liting Blog', 'liting-core');
    }

    public function get_icon() {
        return 'eicon-post';
    }

    public function get_categories() {
        return ['liting'];
    }

    private function get_blog_categories() {
        $options = array();
        $taxonomy = 'category';
        if (!empty($taxonomy)) {
            $terms = get_terms(
                    array(
                        'parent' => 0,
                        'taxonomy' => $taxonomy,
                        'hide_empty' => false,
                    )
            );
            if (!empty($terms)) {
                foreach ($terms as $term) {
                    if (isset($term)) {
                        $options[''] = 'Select';
                        if (isset($term->slug) && isset($term->name)) {
                            $options[$term->slug] = $term->name;
                        }
                    }
                }
            }
        }
        return $options;
    }

    protected function _register_controls() {

        $this->start_controls_section(
                'section_blogs', [
            'label' => esc_html__('Blogs', 'liting-core'),
                ]
        );

        $this->add_control(
                'category_id', [
            'type' => \Elementor\Controls_Manager::SELECT,
            'label' => esc_html__('Category', 'liting-core'),
            'options' => $this->get_blog_categories()
                ]
        );

        $this->add_control(
                'number', [
            'label' => esc_html__('Number of Post', 'liting-core'),
            'type' => Controls_Manager::TEXT,
            'default' => 3
                ]
        );

        $this->add_control(
                'order_by', [
            'label' => esc_html__('Order By', 'liting-core'),
            'type' => Controls_Manager::SELECT,
            'default' => 'date',
            'options' => [
                'date' => esc_html__('Date', 'liting-core'),
                'ID' => esc_html__('ID', 'liting-core'),
                'author' => esc_html__('Author', 'liting-core'),
                'title' => esc_html__('Title', 'liting-core'),
                'modified' => esc_html__('Modified', 'liting-core'),
                'rand' => esc_html__('Random', 'liting-core'),
                'comment_count' => esc_html__('Comment count', 'liting-core'),
                'menu_order' => esc_html__('Menu order', 'liting-core')
            ]
                ]
        );

        $this->add_control(
                'order', [
            'label' => esc_html__('Order', 'liting-core'),
            'type' => Controls_Manager::SELECT,
            'default' => 'desc',
            'options' => [
                'desc' => esc_html__('DESC', 'liting-core'),
                'asc' => esc_html__('ASC', 'liting-core')
            ]
                ]
        );

        $this->add_control(
                'extra_class', [
            'label' => esc_html__('Extra Class', 'liting-core'),
            'type' => Controls_Manager::TEXT
                ]
        );

        $this->end_controls_section();
    }

    protected function render() {
        $settings = $this->get_settings();
        $extra_class = $settings['extra_class'];
        $posts_per_page = $settings['number'];

 
        $order_by = $settings['order_by'];
        $order = $settings['order'];
        $pg_num = get_query_var('paged') ? get_query_var('paged') : 1;
        $args = array(
            'post_type' => array('post'),
            'post_status' => array('publish'),
            'nopaging' => false,
            'paged' => $pg_num,
            'posts_per_page' => $posts_per_page,
            'category_name' => $settings['category_id'],
            'orderby' => $order_by,
            'order' => $order,
        );
        $query = new WP_Query($args);

        ?>

  <!-- news-section -->
    <div class="row">
        <div class="blog-grid blog-1 <?php echo $extra_class; ?>">
        <?php

        if ($query->have_posts()) {
            $i = 1;
            while ($query->have_posts()) {
                $i++;
                $i = $i+1;
                $query->the_post();
        ?>  
        <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="single-blog wow fadeInUp" data-wow-delay="0.<?php echo $i;?>s">
                    <div class="blog-image">
                        <a class="image-scale" href="<?php the_permalink(); ?>">
                        <?php echo the_post_thumbnail(); ?>
                        </a>
                    </div>
                    <div class="blog-content">
                        <?php liting_post_meta();?>
                        <a href="<?php the_permalink(); ?>">
                            <h4><?php the_title(); ?></h4>
                        </a>
                        <?php the_excerpt(); ?>
                        <a class="blog-btn" href="<?php the_permalink(); ?>"><?php esc_html_e('Read more','liting');?></a>
                    </div>
                </div>
            </div>
        </div>
        <?php
            }
            wp_reset_postdata();
        }
        ?>   
        </div>
    </div>


    <!-- news-section end -->
        <?php
    }

    protected function content_template() {
        
    }

}

Plugin::instance()->widgets_manager->register_widget_type(new LitingBlog());
