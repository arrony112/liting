<?php
  use Elementor\Utils;

  class LitingTestimonialSlider extends \Elementor\Widget_Base {

    public function get_name() {
    return 'LitingTestimonialSlider';
  }

  public function get_title() {
    return esc_html__( 'Liting Testimonial Slider', 'liting-core' );
  }

  public function get_icon() {
    return '';
  }

   public function get_categories() {
    return [ 'liting' ];
  }
  
    protected function _register_controls() {

      $this->start_controls_section(
         'content',
         [
           'label' => __( 'Content', 'liting-core' ),
         ]
      );
            $this->add_control(
              'style_select', [
              'label' => esc_html__('Style Select', 'liting-core'),
              'type' => \Elementor\Controls_Manager::SELECT,
              'default' => 'slider',
              'options' => [
                  'slider' => esc_html__('Slider', 'liting-core'),
                  'grid' => esc_html__('Grid', 'liting-core')
                ]
              ]
            );
            $this->add_control(
              'extra_class',
              [
                'label' => __( 'Extra Class', 'liting-core' ),
                'type' => \Elementor\Controls_Manager::TEXT,
                'default' => __( '', 'liting-core' ),
              ]
            );
          $repeater = new \Elementor\Repeater();
          $repeater->add_control(
            'name',
            [
              'label' => __( 'Name', 'liting-core' ),
              'type' => \Elementor\Controls_Manager::TEXT,
              'default' => __( 'Arnold russel', 'liting-core' ),
            ]
          );
          $repeater->add_control(
            'designation',
            [
              'label' => __( 'Designation', 'liting-core' ),
              'type' => \Elementor\Controls_Manager::TEXT,
              'default' => __( 'Genarel customer', 'liting-core' ),
            ]
          );
          $repeater->add_control(
            'image',
            [
              'label' => __( 'Image', 'liting-core' ),
              'type' => \Elementor\Controls_Manager::MEDIA,
              'default' => [
                        'url' => Utils::get_placeholder_image_src(),
                    ],
              
            ]
          );
          $repeater->add_control(
            'content',
            [
              'label' => __( 'Content', 'liting-core' ),
              'type' => \Elementor\Controls_Manager::TEXTAREA,
            ]
          );
          $repeater->add_control(
            'rating', [
            'label' => esc_html__('Rating Select', 'liting-core'),
            'type' => \Elementor\Controls_Manager::SELECT,
            'default' => '5',
            'options' => [
                '5' => esc_html__('5', 'liting-core'),
                '4' => esc_html__('4', 'liting-core'),
                '3' => esc_html__('3', 'liting-core'),
                '2' => esc_html__('2', 'liting-core'),
                '1' => esc_html__('1', 'liting-core'),
              ]
            ]
          );
          
      $this->end_controls_section();

      $this->start_controls_section(
        'testimonial_list',
        [
          'label' => __( 'Testimonial List', 'liting-core' ),
        ]
      );
      $this->add_control(
        'items1',
        [
          'label' => __( 'Repeater List', 'liting-core' ),
          'type' => \Elementor\Controls_Manager::REPEATER,
          'fields' => $repeater->get_controls(),
          'default' => [
            [
              'list_title' => __( 'Title #1', 'liting-core' ),
              'list_content' => __( 'Item content. Click the edit button to change this text.', 'liting-core' ),
            ],
            [
              'list_title' => __( 'Title #2', 'liting-core' ),
              'list_content' => __( 'Item content. Click the edit button to change this text.', 'liting-core' ),
            ],
          ],
        ]
      );
  
      $this->end_controls_section();
  
    }    
    protected function render() {
      $settings =  $this->get_settings_for_display();  
      $extra_class = $settings["extra_class"]; 
      $style_select = $settings["style_select"]; 
      
?>

<?php if($style_select == "slider"){?>
          <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="Reviews-content wow fadeInUp" data-wow-delay="0.3s">
                        <!-- start testimonial carousel -->
                        <div class="testimonial-carousel item-indicator">
                        <?php 
                        foreach($settings["items1"] as $item){ 
                          $name = $item["name"]; 
                          $designation = $item["designation"]; 
                          $content = $item["content"]; 
                          $image = $item[ 'image']['url']; 
                          $rating = $item[ 'rating']; 
                          ?>
                          <div class="single-testi">
                              <div class="testi-text">
                                  <div class="clients-text">
                                      <div class="client-rating rating_no_<?php echo esc_attr($rating);?>">
                                          <i class="icon icon-star"></i>
                                          <i class="icon icon-star"></i>
                                          <i class="icon icon-star"></i>
                                          <i class="icon icon-star"></i>
                                          <i class="icon icon-star"></i>
                                      </div>
                                      <p><?php echo esc_html($content);?></p>
                                  </div>
                                  <div class="testi-img">
                                      <img src="<?php echo esc_url($image);?>" alt="">
                                      <div class="guest-details">
                                          <h4><?php echo esc_html($name);?></h4>
                                          <span class="guest-rev"><?php echo esc_html_e('Clients','liting-core');?> - <span><?php echo esc_html($designation);?></span></span>
                                      </div>
                                  </div>
                              </div>
                          </div>
                        <?php } ?>
                      </div>
                  </div>
              </div>
          </div>
<?php }else{ ?>
              <div class="row">
                  <div class="Reviews-content wow fadeInUp" data-wow-delay="0.3s">
                        <!-- start testimonial carousel -->
                       
                        <?php 
                        foreach($settings["items1"] as $item){ 
                          $name = $item["name"]; 
                          $designation = $item["designation"]; 
                          $content = $item["content"]; 
                          $image = $item[ 'image']['url']; 
                          ?>
                          <div class="col-md-4 col-sm-6 col-xs-12">
                              <div class="single-testi">
                                  <div class="testi-text">
                                      <div class="clients-text">
                                          <div class="client-rating rating_no_<?php echo esc_attr($rating);?>">
                                              <i class="icon icon-star"></i>
                                              <i class="icon icon-star"></i>
                                              <i class="icon icon-star"></i>
                                              <i class="icon icon-star"></i>
                                              <i class="icon icon-star"></i>
                                          </div>
                                          <p><?php echo esc_html($content);?></p>
                                      </div>
                                      <div class="testi-img">
                                          <img src="<?php echo esc_url($image);?>" alt="">
                                          <div class="guest-details">
                                              <h4><?php echo esc_html($name);?></h4>
                                              <span class="guest-rev"><?php echo esc_html_e('Clients','liting-core');?> - <span><?php echo esc_html($designation);?></span></span>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                        <?php } ?>
                   
                  </div>
            </div>
<?php } ?>

 <?php 
    }
  
    protected function _content_template() {
      
    }
  }

\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \LitingTestimonialSlider() );