<?php
  use Elementor\Utils;

  class LitingServiceBox extends \Elementor\Widget_Base {

    public function get_name() {
    return 'LitingServiceBox';
  }

  public function get_title() {
    return esc_html__( 'Liting Service Box', 'liting-core' );
  }

  public function get_icon() {
    return '';
  }

   public function get_categories() {
    return [ 'liting' ];
  }
  
    protected function _register_controls() {

      $this->start_controls_section(
         'content',
         [
           'label' => __( 'Content', 'liting-core' ),
         ]
      );
            $this->add_control(
              'extra_class',
              [
                'label' => __( 'Extra Class', 'diaco' ),
                'type' => \Elementor\Controls_Manager::TEXT,
                'default' => __( '', 'liting-core' ),
              ]
            );
          $repeater = new \Elementor\Repeater();
          $repeater->add_control(
            'title',
            [
              'label' => __( 'Title', 'liting-core' ),
              'type' => \Elementor\Controls_Manager::TEXT,
              'default' => __( 'Strategy & Analytics', 'liting-core' ),
            ]
          );
          $repeater->add_control(
            'content',
            [
              'label' => __( 'Content', 'liting-core' ),
              'type' => \Elementor\Controls_Manager::TEXTAREA,
              'default' => __( 'Aspernatur sit adipisci quaerat unde at neque Redug Lagre dolor sit amet consectetu.', 'liting-core' ),
            ]
          );
          $repeater->add_control(
            'icon',
            [
              'label' => __( 'Icon', 'liting-core' ),
              'type' => \Elementor\Controls_Manager::ICON,
            ]
          );
          $repeater->add_control(
            'url',
            [
                'label' => __( 'Url', 'liting-core' ),
                'type' => \Elementor\Controls_Manager::URL,
                'placeholder' => __( 'https://your-link.com', 'liting-core' ),
                'show_external' => true,
                'default' => [
                  'url' => '',
                  'is_external' => true,
                  'nofollow' => true,
                ],
                
              ]
            );
      $this->end_controls_section();

      $this->start_controls_section(
        'service_list',
        [
          'label' => __( 'Service Box List', 'liting-core' ),
        ]
      );
      $this->add_control(
        'items1',
        [
          'label' => __( 'Repeater List', 'liting-core' ),
          'type' => \Elementor\Controls_Manager::REPEATER,
          'fields' => $repeater->get_controls(),
          'default' => [
            [
              'list_title' => __( 'Title #1', 'liting-core' ),
              'list_content' => __( 'Item content. Click the edit button to change this text.', 'liting-core' ),
            ],
            [
              'list_title' => __( 'Title #2', 'liting-core' ),
              'list_content' => __( 'Item content. Click the edit button to change this text.', 'liting-core' ),
            ],
          ],
        ]
      );
  
      $this->end_controls_section();
  
    }    
    protected function render() {
      $settings =  $this->get_settings_for_display(); 
      $extra_class = $settings["extra_class"]; 
      
?>

<div class="row">
    <div class="all-services <?php echo esc_attr($extra_class);?>">
          <!-- single-services-->
          <?php 
          $i = 1;
          foreach($settings["items1"] as $item){ 
            $i++;
            $i = $i+1;
            $title = $item["title"]; 
            $content = $item["content"]; 
            $icon = $item["icon"]; 
            $url = $item[ 'url']['url']; 
            ?>
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="well-services wow fadeInUp" data-wow-delay="0.<?php echo $i;?>s">
                  <div class="services-img">
                    <a class="big-icon" href="<?php echo esc_url($url);?>"><i class="<?php echo $icon;?>"></i></a>
                  </div>
                  <div class="main-wel">
                    <div class="wel-content">
                      <h4><?php echo $title;?></h4>
                      <p><?php echo $content;?></p>
                    </div>
                  </div>
                </div>
              </div>
          <?php } ?>
    </div>
</div>

 <?php 
    }
  
    protected function _content_template() {
      
    }
  }

\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \LitingServiceBox() );