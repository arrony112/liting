<?php
  use Elementor\Utils;

  class LitingBanner extends \Elementor\Widget_Base {

    public function get_name() {
    return 'LitingBanner';
  }

  public function get_title() {
    return esc_html__( 'Liting Banner', 'liting-core' );
  }

  public function get_icon() {
    return '';
  }

   public function get_categories() {
    return [ 'liting' ];
  }
  
    protected function _register_controls() {

      $this->start_controls_section(
         'content',
         [
           'label' => __( 'Content', 'liting-core' ),
         ]
      );
      $this->add_control(
        'image',
        [
          'label' => __( 'Banner Image', 'liting-core' ),
          'type' => \Elementor\Controls_Manager::MEDIA,
          'default' => [
                    'url' => Utils::get_placeholder_image_src(),
                ],
          
        ]
      );
      $this->add_control(
        'image2',
        [
          'label' => __( 'Banner Image 2', 'liting-core' ),
          'type' => \Elementor\Controls_Manager::MEDIA,
          'default' => [
                    'url' => Utils::get_placeholder_image_src(),
                ],
          
        ]
      );
          $this->add_control(
            'sub_title',
            [
              'label' => __( 'Sub Title', 'liting-core' ),
              'type' => \Elementor\Controls_Manager::TEXT,
            ]
          );
          $this->add_control(
            'title',
            [
              'label' => __( 'Title', 'liting-core' ),
              'type' => \Elementor\Controls_Manager::TEXTAREA,
            ]
          );
          $this->add_control(
            'desc',
            [
              'label' => __( 'Description', 'liting-core' ),
              'type' => \Elementor\Controls_Manager::TEXTAREA,
            ]
          );
          $this->add_control(
            'button_url',
            [
                'label' => __( 'Button Url', 'liting-core' ),
                'type' => \Elementor\Controls_Manager::URL,
                'placeholder' => __( 'https://your-link.com', 'liting-domain' ),
                'show_external' => true,
                'default' => [
                  'url' => '',
                  'is_external' => true,
                  'nofollow' => true,
                ],
                
              ]
            );
            $this->add_control(
              'extra_class',
              [
                'label' => __( 'Extra Class', 'liting-core' ),
                'type' => \Elementor\Controls_Manager::TEXT,
                'default' => __( '', 'liting-core' ),
              ]
            );
  
      $this->end_controls_section();

  
    }    
    protected function render() {
      $settings =  $this->get_settings_for_display(); 
      $extra_class = $settings["extra_class"]; 
      $sub_title = $settings["sub_title"];
      $title = $settings["title"];
      $desc = $settings["desc"];
      $image = $settings["image"]["url"]; 
      $image2 = $settings["image2"]["url"]; 
      $button_url = $settings["button_url"]["url"];  
?>
    <div class="intro-area <?php echo esc_attr($extra_class);?>">
        <div class="bg-wrapper">
          <img src="<?php echo esc_url($image);?>" alt="">
        </div>
        <div class="intro-bg">
          <img src="<?php echo esc_url($image2);?>" alt="">
        </div>
        <div class="intro-content">
          <div class="slider-content">
            <div class="container">
              <div class="row">
                <div class="col-md-12">
                  <!-- layer 1 -->
                  <div class="layer-1 wow fadeInUp" data-wow-delay="0.3s">
                      <h6 class="best-title"><span class="slide-icon"><i class="icon icon-layers"></i></span> <?php echo esc_html($sub_title);?></h6>
                    <h2 class="title2"><?php echo wp_kses_post($title);?></h2>
                  </div>
                  <!-- layer 2 -->
                  <div class="layer-2 wow fadeInUp" data-wow-delay="0.5s">
                    <p><?php echo wp_kses_post($desc);?></p>
                  </div>
                  <!-- layer 3 -->
                  <div class="layer-3 wow fadeInUp" data-wow-delay="0.7s">
                    <a href="<?php echo esc_url($button_url);?>" class="ready-btn left-btn" ><?php esc_html_e('Services','liting-core');?></a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
 <?php 
    }
  
    protected function _content_template() {
      
    }
  }

\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \LitingBanner() );