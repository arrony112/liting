<?php

namespace Liting;

class Element {

    public function __construct() {
       
        add_action('elementor/widgets/widgets_registered', array($this, 'widgets_registered'));
    }

    public function widgets_registered() {
      
        if (defined('LITING_ELEMENTOR_PATH') && class_exists('Elementor\Widget_Base')) {
            require_once LITING_ELEMENTOR_INCLUDES . '/widgets/liting-banner.php';
            require_once LITING_ELEMENTOR_INCLUDES . '/widgets/liting-service-box.php';
            require_once LITING_ELEMENTOR_INCLUDES . '/widgets/liting-service.php';
            require_once LITING_ELEMENTOR_INCLUDES . '/widgets/liting-service-video.php';
            require_once LITING_ELEMENTOR_INCLUDES . '/widgets/liting-team.php';
            require_once LITING_ELEMENTOR_INCLUDES . '/widgets/liting-testimonial-slider.php';
            require_once LITING_ELEMENTOR_INCLUDES . '/widgets/liting-blogs.php';
            require_once LITING_ELEMENTOR_INCLUDES . '/widgets/liting-contact.php';
            require_once LITING_ELEMENTOR_INCLUDES . '/widgets/liting-price-box.php';
            require_once LITING_ELEMENTOR_INCLUDES . '/widgets/liting-heading.php';
            require_once LITING_ELEMENTOR_INCLUDES . '/widgets/liting-counter.php';
            require_once LITING_ELEMENTOR_INCLUDES . '/widgets/liting-faq.php';

        }
    }

}
