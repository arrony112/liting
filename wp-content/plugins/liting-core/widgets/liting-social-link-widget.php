<?php

class LitingSocialLink extends WP_Widget {

// Main constructor
    public function __construct() {
        parent::__construct(
                'liting_custom_social_link', __('Liting Social Link', 'liting-core'), array(
            'customize_selective_refresh' => true,
                )
        );
    }

    public function form($instance) {

        $defaults = array(
            'title' => '',
            'facebook' => '',
            'twitter' => '',
            'google' => '',
            'pinterest' => '',
            'instagram' => '',
        );

        extract(wp_parse_args((array) $instance, $defaults));
        ?>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php _e('Widget Title', 'liting-core'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>

        <p>
            <label for="<?php echo esc_attr($this->get_field_id('facebook')); ?>"><?php _e('Facebook Url', 'liting-core'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('facebook')); ?>" name="<?php echo esc_attr($this->get_field_name('facebook')); ?>" type="text" value="<?php echo esc_attr($facebook); ?>" />
        </p>

        <p>
            <label for="<?php echo esc_attr($this->get_field_id('twitter')); ?>"><?php _e('Twitter Url', 'liting-core'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('twitter')); ?>" name="<?php echo esc_attr($this->get_field_name('twitter')); ?>" type="text" value="<?php echo esc_attr($twitter); ?>" />
        </p>

        <p>
            <label for="<?php echo esc_attr($this->get_field_id('google')); ?>"><?php _e('Google Plus Url', 'liting-core'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('google')); ?>" name="<?php echo esc_attr($this->get_field_name('google')); ?>" type="text" value="<?php echo esc_attr($google); ?>" />
        </p>


        <p>
            <label for="<?php echo esc_attr($this->get_field_id('pinterest')); ?>"><?php _e('Pinterest Url', 'liting-core'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('pinterest')); ?>" name="<?php echo esc_attr($this->get_field_name('pinterest')); ?>" type="text" value="<?php echo esc_attr($pinterest); ?>" />
        </p>


        <p>
            <label for="<?php echo esc_attr($this->get_field_id('instagram')); ?>"><?php _e('Instagram Url', 'liting-core'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('instagram')); ?>" name="<?php echo esc_attr($this->get_field_name('instagram')); ?>" type="text" value="<?php echo esc_attr($instagram); ?>" />
        </p>


        <?php
    }

    public function update($new_instance, $old_instance) {
        $instance = $old_instance;
        $instance['title'] = isset($new_instance['title']) ? wp_strip_all_tags($new_instance['title']) : '';
        $instance['facebook'] = isset($new_instance['facebook']) ? wp_strip_all_tags($new_instance['facebook']) : '';
        $instance['twitter'] = isset($new_instance['twitter']) ? wp_strip_all_tags($new_instance['twitter']) : '';
        $instance['google'] = isset($new_instance['google']) ? wp_strip_all_tags($new_instance['google']) : '';
        $instance['pinterest'] = isset($new_instance['pinterest']) ? wp_strip_all_tags($new_instance['pinterest']) : '';
        $instance['instagram'] = isset($new_instance['instagram']) ? wp_strip_all_tags($new_instance['instagram']) : '';
        return $instance;
    }

    public function widget($args, $instance) {

        extract($args);
        $title = isset($instance['title']) ? apply_filters('widget_title', $instance['title']) : '';
        $facebook = isset($instance['facebook']) ? $instance['facebook'] : '';
        $twitter = isset($instance['twitter']) ? $instance['twitter'] : '';
        $google = isset($instance['google']) ? $instance['google'] : '';
        $pinterest = isset($instance['pinterest']) ? $instance['pinterest'] : '';
        $instagram = isset($instance['instagram']) ? $instance['instagram'] : '';
        ?>
        <?php if(!empty($title)){?>
        <h2><?php echo $title;?></h2>
        <?php } ?>
        <div class="footer-icons">
            <ul>
                <?php if(!empty($facebook)){?>
                <li><a href="<?php echo esc_url($facebook); ?>"><i class="fa fa-facebook"></i></a></li>
                <?php } ?>
                <?php if(!empty($twitter)){?>
                <li><a href="<?php echo esc_url($twitter); ?>"><i class="fa fa-twitter"></i></a></li>
                <?php } ?>
                <?php if(!empty($google)){?>
                <li><a href="<?php echo esc_url($google); ?>"><i class="fa fa-google"></i></a></li>
                <?php } ?>
                <?php if(!empty($pinterest)){?>
                <li><a href="<?php echo esc_url($pinterest); ?>"><i class="fa fa-pinterest"></i></a></li>
                <?php } ?>
                <?php if(!empty($instagram)){?>
                <li><a href="<?php echo esc_url($instagram); ?>"><i class="fa fa-instagram"></i></a></li>
                <?php } ?>
            </ul>
        </div>
        <?php
    }

}

function liting_social_network() {
    register_widget('LitingSocialLink');
}

add_action('widgets_init', 'liting_social_network');
?>